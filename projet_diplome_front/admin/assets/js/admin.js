//class Lieu
var Lieu = function(){
    this.searchLieu = function(form, inputSearch, callback){
      var regEx = "\s";
      inputSearch = inputSearch.replace(/ /g, "+");
      var results = {};
      var url = "https://api.foursquare.com/v2/venues/search?client_id=YJY5REYKOY4UV0HCHLXMME5EHDLEC14VCYCK5FJAL1ECBV1Q&client_secret=DNURKSXJXEX4RGPXF0CQ2EOLYGQJDOCJFRNVENVNPTUT2SCS&intent=global&query="+inputSearch+"&v=20200115";
      var request = new XMLHttpRequest();
      request.open("GET", url);
      request.send();
      request.onreadystatechange = function() {
        if(request.readyState === 4){
          if(request.status === 200) {
            var results = JSON.parse(request.responseText);
            var lieux = results['response']['venues'];

            var tableResults = document.getElementById('tableResults');
            for(var i=0; i < lieux.length; i++){
              var tr = document.createElement('tr');
              tr.setAttribute('data-id',lieux[i].id);
              tableResults.appendChild(tr);
              tableInfos = [lieux[i].name, lieux[i].location.address, lieux[i].location.city, lieux[i].location.country, lieux[i].categories[0].name, "Ajouter"];

              callback(tableInfos,tr,createTd);
            }
          }
          else {
            alert ('Problème JSON');
          }
        }
      }
  }


  this.addLieu = function(lieu){
    var data_id = lieu.parentNode.parentNode.getAttribute("data-id");
    var results = {};
    var url = "https://api.foursquare.com/v2/venues/data_id?client_id=YJY5REYKOY4UV0HCHLXMME5EHDLEC14VCYCK5FJAL1ECBV1Q&client_secret=DNURKSXJXEX4RGPXF0CQ2EOLYGQJDOCJFRNVENVNPTUT2SCS&v=20200115";
    var request = new XMLHttpRequest();
    request.open("GET", url);
    request.send();
    request.onreadystatechange = function() {
      if(request.readyState === 4){
        if(request.status === 200) {
          var results = JSON.parse(request.responseText);
          tableInfos = [
            "id" => results["response"]["venue"]["id"],
            "name" => results["response"]["venue"]["name"],
            "description" => results["response"]["venue"]["description"],
            "latitude" => results["response"]["venue"]["location"]["lat"],
            "longitude" => results["response"]["venue"]["location"]["long"],
            "adresse" => results["response"]["venue"]["location"]["address"],
            "heures_ouv" => results["response"]["venue"]["timeframes"]["open"]["renderedTime"],
            "tarif" => results["response"]["venue"]["id"],
            "ville" => results["response"]["venue"]["location"]["city"],
            "pays" => results["response"]["venue"]["location"]["country"],
            "categorie" => results["response"]["venue"]["categories"]
          ]
        }
        else {
          alert ('Problème JSON');
        }
      }
    }
  }
}

var lieu = new Lieu();

document.getElementById('formSearchLieu').addEventListener('submit', function(e){
  e.preventDefault();
  var inputSearch = document.getElementById('inputSearch').value;
  lieu.searchLieu(this, inputSearch, appendTd);
});

function appendTd(infos, tr, callback){
  for(var i=0; i<infos.length;i++){
    callback(infos[i], tr);
  }
}

function createTd(info, tr){
  var td = document.createElement('td');
  tr.appendChild(td);

  if(info != "Ajouter"){
    var tdText = document.createTextNode(info);
    td.appendChild(tdText);
  }
  else {
    var btn = document.createElement('button');
    btn.setAttribute("class","addLieu");
    //btn.setAttribute("onclick", "addLieu()");
    btn.innerHTML = info;
    td.appendChild(btn);

    btn.addEventListener('click', function(e){
      e.preventDefault();
      lieu.addLieu(this);
    });
  }
}
