<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Rechercher un lieu</title>
  </head>
  <body>
    <form id="formSearchLieu" method="get">
      <input type="text" name="search" id="inputSearch" />
      <button class="addLieu" type="submit" name="button">Rechercher</button>
    </form>
    <table id="tableResults">
      <tr>
        <th>Nom</th>
        <th>Adresse</th>
        <th>Ville</th>
        <th>Pays</th>
        <th>Catégorie</th>
      </tr>
    </table>
    <script type="text/javascript" src="assets/js/admin.js"></script>
  </body>
</html>
