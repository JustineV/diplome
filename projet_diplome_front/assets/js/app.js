//class Voyage for all function which do ajax
var Voyage = function() {

  //search city search by the user
  this.searchVilles = function(url, ville){
    ville = ville.toLowerCase();
    $.ajax({
      url:url,
      type:"GET",
      data:
        {
          "ville":ville,
          "token": localStorage.getItem('token')
        },
      dataType: "json",
      success: function(response,statut)
      {
        var html = "";
        $.each(response,function(index, element){
          html += `<div class="polaroid polaroidLieu" data-id="`+element['id']+`" data-nom="`+element['nom']+`">
              <div class="imagePolaroid" style="background-image:url('`+element['url_photo']+`');" ></div>
              <p>`+element['nom']+`</p>
            </div>`;
        });
        $('#resultatsLieux').html(html);
        $('.polaroidLieu').click(function(){
          $(this).toggleClass('is-active');
          $(".etapes-recherche i:nth-child(2)").removeClass('icon-lens-outline').addClass('icon-lens');
          $("#inputSearchVille").toggleClass('is-hidden');
          $('#resultatsLieux').toggleClass('is-hidden');
          $('#typesCreneaux').toggleClass('is-hidden');
          $('.swiper-button-prev').toggleClass('is-hidden');
          $('.swiper-button-next').toggleClass('is-hidden');

          // button back to
          $('nav .back').toggleClass('opacity');
          $('nav .back').attr('data-section', 'creneau');
        });
      }
    });
  }

  //search tendance city
  this.searchVillesTendances = function(url){
    $.ajax({
      url:url,
      type:"GET",
      dataType: "json",
      success: function(response,statut)
      {
        $('.loader-wrapper').toggleClass('is-active');
        $('.loader-wrapper').toggleClass('is-hidden');
        var html = "<p class='title brown is-uppercase'>Nos Tendances</p>";
        $.each(response,function(index, element){
          html += `
            <div class="polaroid polaroidLieu" data-id="`+element['id']+`" data-nom="`+element['nom']+`">
              <div class="imagePolaroid" style="background-image:url('`+element['url_photo']+`');" ></div>
              <p>`+element['nom']+`</p>
            </div>
          `;
        });
        $('#resultatsLieux').html(html);
        $('.polaroidLieu').click(function(event){
          $(this).toggleClass('is-active');
          $(".etapes-recherche i:nth-child(2)").removeClass('icon-lens-outline').addClass('icon-lens');
          $("#inputSearchVille").toggleClass('is-hidden');
          $('#resultatsLieux').toggleClass('is-hidden');
          $('#typesCreneaux').toggleClass('is-hidden');
          $('nav .back').attr('data-section','creneau');
          $('nav .back').toggleClass('opacity');
        });
      }
    });
  }

  //search all thematiques in db
  this.getThematiquesList = function(url, destinationElement){
    var checkbox, label;
    var request = new XMLHttpRequest();
    request.open("GET", url);
    request.addEventListener("load", function(){
      var thematiquesData = JSON.parse(request.responseText);

      if(thematiquesData.length != undefined){
        var html = "";
        for(var i=0; i < thematiquesData.length; i++){
          html += `
          <div class="thematique column" data-id="`+thematiquesData[i]["id"]+`">
            <i class="dark-blue icon-`+thematiquesData[i]['icon']+`"></i>
            <p class="dark-blue is-uppercase">`+thematiquesData[i]['nom']+`</p>
          </div>
          `;
        }
        $('#listThematiques').html(html);

        $('#listThematiques > div').click(function(){
          $(this).toggleClass('is-active');
        });
      }

    });
    request.send();
  }

  counterError = 0;
  //User has enter data search lieu or verified if he finished
  this.searchProgram = function(url, ville, duree, creneauxDemandes, thematiquesChecked, lieuAdded, dureeVisite, creneauSelected){
    $('.loader-wrapper').toggleClass('is-active');
    $('.loader-wrapper').toggleClass('is-hidden');
    var voyage = this;

    $.ajax({
      url:url,
      type:"GET",
      data:
        {
          "ville":ville,
          "duree":duree,
          "creneaux":creneauxDemandes,
          "thematiques":thematiquesChecked,
          "lieuAdded": lieuAdded,
          "dureeVisite": dureeVisite,
          "creneauSelected": creneauSelected,
          "token": localStorage.getItem('token'),
          "v":"20200115",
        },
      dataType: "json",
      success: function(response,statut)
      {
        counter = 0;
        $('.loader-wrapper').toggleClass('is-active');
        $('.loader-wrapper').toggleClass('is-hidden');
        $('#listLieux').empty();

        if(response['response'] == "Program complete"){
          console.log('-- PROGRAM COMPLETED --');
          voyage.seeProgram(ville, response['voyage_id']);
        }
        else if(response['response'] == "Too much calls"){
          console.log('-- TOO MUCH CALLS --');
          alert('Nous manquons encore de données sur cette destination et/ou thématiques, veuillez essayer avec un nouvelle recherche');
          voyage.seeProgram(ville, response['voyage_id']);
        }
        else if(response['response'] == "No data"){
          alert('Nous manquons encore de données sur cette destination et/ou thématiques, veuillez essayer avec un nouvelle recherche');
          voyage.seeProgram(ville, response['voyage_id']);
        }
        else {
          $('#listLieux').append("<div class='swiper-wrapper'></div>");
          counterDate = 0;
          $.each(response,function(index, element){
            var nomLieu = element['json']['name'];
            if(!element['json']['bestPhoto']){
              if(element['json']['photos']['groups'][0] != undefined){
                var photoLieu = element['json']['photos']['groups'][0]['items'][0].prefix+"500x500"+element['json']['photos']['groups'][0]['items'][0].suffix;
              }
              else {
                var photoLieu = "";
              }
            } else {
              var photoLieu = element['json']['bestPhoto'].prefix+"500x500"+element['json']['bestPhoto'].suffix;
            }
            var descriptionLieu = (element['json']['description'] != undefined) ? '<div><p class="text-border">Description :</p><p>'+element['json']['description']+'</p></div>' : '';
            var tarifLieu = (element['json']['price'] != undefined) ? '<div><p class="text-border">Tarif :</p><p>'+element['json']['price']+'</p></div>' : '';
            var siteWeb = (element['json']['url'] != undefined) ? '<div><p class="text-border">Site web :</p><p><a href="'+element['json']['url']+'" target="_blank">'+element['json']['url']+'</a></p></div>' : '';
            var adresseLieu = (element['json']['location']['lat'] != undefined) ? '<div><p class="text-border">Adresse :</p><iframe width="300" height="170" src="https://maps.google.com/maps?q='+element['json']['location']['lat']+','+element['json']['location']['lng']+'&hl=fr&z=14&amp;output=embed"></iframe></div>' : '';
            var horaires = "";
            for(var h=0; h < element['horaires_dispos']['verification'].length; h++){
              if(element['horaires_dispos']['verification'][h] == true){
                if(h==1 && element['horaires_dispos']['verification'][h] == true){
                  horaires += "<button value='3600'class='selected'>1h</button>";
                }
                else if(h==2 && element['horaires_dispos']['verification'][h] == true){
                  if(element['horaires_dispos']['verification_option'][h] == true){
                    horaires +=  "<button value='7200'>2h</button>";
                  }else {
                    horaires +=  "<button value='7200' class='optionnal'>2h</button>";
                  }
                }
                else if(h==3 && element['horaires_dispos']['verification'][h] == true){
                  if(element['horaires_dispos']['verification_option'][h] == true){
                    horaires += "<button value='14400'>1/2j</button>";
                  }else {
                    horaires += "<button value='14400' class='optionnal'>1/2j</button>";
                  }
                }
              }
            }

            //create each slide
            var text = `
            <div class='polaroid polaroidVisite lieu swiper-slide' data-id="`+element['id']+`" data-idfoursquare="`+element['id_foursquare']+`" data-creneau="`+element['creneau_selected']+`">
              <div class='frontLieu'>
                <div class='imagePolaroid' style='background-image:url("`+photoLieu+`")'></div>
                <p>`+nomLieu+`</p>
                <div class='voir-plus'></div>
                <a class='button btn-brown'><i class='icon-add'></i></a></div>
              </div>
              <div class='backLieu is-hidden' data-id="`+element['id']+`">
                <div class='background-lieu' style='background-image:url("`+photoLieu+`")' ></div>
                <p class="title dark-blue">`+nomLieu+`</p>
                <div class='other-infos'>
                  <div class='has-text-left duree-visite'>
                    <p class="text-border">Durée de la visite :</p>
                    <div>`+ horaires +`</div>
                  </div>
                  `+descriptionLieu+tarifLieu+siteWeb+adresseLieu+`
                </div>
              </div>
            </div>`;

            $('#listLieux .swiper-wrapper').append(text);
            $('#chooseLieu').append('<div class="swiper-button-prev"></div><div class="swiper-button-next"></div>');
            var indiceCreneau = element['creneau_selected'];
            var splitIndiceCreneau = creneauxDemandes[indiceCreneau*1][0].split(' ');
            if(counterDate == 0){
              $('.infosCreneau .creneauDate').html('<i class="icon-calendar-alt-regular"></i>'+splitIndiceCreneau[1]);
              $('.infosCreneau .creneauHoraire').html('<i class="icon-clock-regular"></i>A partir '+response[0]['time_deb']);
            }
            else if(counterDate == 2){
              /*$('.polaroidVisite .button').click(function(){
                var parent = $(this).parent().parent();
                var frontLieu = $(this).parent();
                frontLieu.toggleClass('is-hidden');
                $('#listLieux .polaroid').toggleClass('is-hidden');
                var dataId = parent[0].getAttribute('data-id');
                var backLieu = $('.backLieu');
                $.each(backLieu, function(index, element){
                  if(element.getAttribute('data-id') == dataId){
                    $(this).toggleClass('is-hidden');
                  }
                });
                $('nav .back').attr('data-section', 'backLieu');
                $('#chooseLieu .title').toggleClass('is-hidden');
                $('#chooseLieu .infosCreneau').toggleClass('is-hidden');
                $('#chooseLieu .backLieu .title').toggleClass('is-hidden');
                $('#chooseLieu .swiper-button-prev').toggleClass('is-hidden');
                $('#chooseLieu .swiper-button-next').toggleClass('is-hidden');
                $('#listLieux').toggleClass('backLieu-open');
                $('nav .back').toggleClass('opacity');
              });*/

              // see all infos of the place
              $('.polaroidVisite').click(function(){
                var frontLieu = $(this).children('.frontLieu');
                frontLieu.toggleClass('is-hidden');
                $('#listLieux .polaroid').toggleClass('is-hidden');
                var dataId = $(this).attr('data-id');
                var backLieu = $('.backLieu');
                $.each(backLieu, function(index, element){
                  if(element.getAttribute('data-id') == dataId){
                    $(this).toggleClass('is-hidden');
                  }
                });
                $('nav .back').attr('data-section', 'backLieu');
                $('#chooseLieu .title').toggleClass('is-hidden');
                $('#chooseLieu .infosCreneau').toggleClass('is-hidden');
                $('#chooseLieu .backLieu .title').toggleClass('is-hidden');
                $('#chooseLieu .swiper-button-prev').toggleClass('is-hidden');
                $('#chooseLieu .swiper-button-next').toggleClass('is-hidden');
                $('#listLieux').toggleClass('backLieu-open');
                $('nav .back').toggleClass('opacity');
              });


              $('.backLieu .duree-visite button').click(function(){
                $('.backLieu .duree-visite button.selected').toggleClass('selected');
                $(this).toggleClass('selected');
              });

            }
            counterDate++;
          });

          // init slider
          var mySwiper = new Swiper ('.swiper-container', {
              // Optional parameters
              direction: 'horizontal',
              centeredSlides: true,
              slidesPerView: 1,
              height: 300,
              spaceBetween:30,

              // Navigation arrows
              navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
              },
            })
        }
      },
      error: function(resultat, statut, erreur){
        console.log('erreur');
        console.log(erreur);
        console.log(resultat);
        console.log(statut);
        alert('Une erreur s\'est produite merci de réessayer');
      },
      complete: function(resultat, statut){
        console.log('complete');
        console.log(resultat);
        console.log(statut);
      }
    });
  }

  // see program with all places chose
  this.seeProgram = function(ville, voyage_id){
    $('.loader-wrapper').toggleClass('is-active');
    $('.loader-wrapper').toggleClass('is-hidden');

    var url = "https://www.justine-vincent.fr/echappee/projet_diplome/public/index.php/program/list";
    $.ajax({
      url:url,
      type:"GET",
      data:
        {
          "voyage_id":voyage_id
        },
      dataType: "json",
      success: function(response,statut)
      {
        $('.loader-wrapper').toggleClass('is-active');
        $('.loader-wrapper').toggleClass('is-hidden');
        $('#listProgram').append("<p class='title brown is-uppercase has-text-centered'>Votre programme à "+ville+"</p>");
        $('#chooseLieu').toggleClass('is-hidden');

        $.each(response,function(index, element){
          var date_deb = new Date(element['heure_deb']['timestamp']*1000);
          var date_fin = new Date(element['heure_fin']['timestamp']*1000);
          var jour = date_deb.getDate()+"/"+(parseFloat(date_deb.getMonth())+1)+"/"+date_deb.getFullYear();
          if(date_deb.getMinutes() != 0){
            var minutes_deb = date_deb.getMinutes();
          }
          else {
            var minutes_deb = "";
          }
          if(date_fin.getMinutes() != 0){
            var minutes_fin = date_fin.getMinutes();
          }
          else {
            var minutes_fin = "";
          }
          var heure_deb = (date_deb.getHours())+"h"+minutes_deb;
          var heure_fin = (date_fin.getHours())+"h"+minutes_fin;

          var nomLieu = element['lieu']['nom'];
          if(!element['lieu']['json']['bestPhoto']){
            if(element['lieu']['json']['photos']['groups'][0] != undefined){
              var photoLieu = element['lieu']['json']['photos']['groups'][0]['items'][0].prefix+"500x500"+element['lieu']['json']['photos']['groups'][0]['items'][0].suffix;
            }
            else {
              var photoLieu = "";
            }
          } else {
            var photoLieu = element['lieu']['json']['bestPhoto'].prefix+"500x500"+element['lieu']['json']['bestPhoto'].suffix;
          }
          var descriptionLieu = (element['lieu']['json']['description'] != undefined) ? '<div><p class="text-border">Description :</p><p>'+element['lieu']['json']['description']+'</p></div>' : '';
          var tarifLieu = (element['lieu']['json']['price'] != undefined) ? '<div><p class="text-border">Tarif :</p><p>'+element['lieu']['json']['price']+'</p></div>' : '';
          var siteWeb = (element['lieu']['json']['url'] != undefined) ? '<div><p class="text-border">Site web :</p><p><a href="'+element['lieu']['json']['url']+'" target="_blank">'+element['lieu']['json']['url']+'</a></p></div>' : '';
          var adresseLieu = (element['lieu']['json']['location']['lat'] != undefined) ? '<div><p class="text-border">Adresse :</p><iframe width="300" height="170" src="https://maps.google.com/maps?q='+element['lieu']['json']['location']['lat']+','+element['lieu']['json']['location']['lng']+'&hl=fr&z=14&amp;output=embed"></iframe></div>' : '';

          var lieu = `<div class='creneau'>
            <div class='infosCreneau'>
              <p class="brown"><i class='icon-calendar-alt-regular'></i>`+jour+`</p>
              <p class="brown"><i class='icon-clock-regular'></i>`+heure_deb+`-`+heure_fin+`</p>
            </div>
            <div class='polaroid polaroidVisite lieu' data-id="`+element['lieu']['id']+`" data-idfoursquare="`+element['lieu']['id_foursquare']+`">
              <div class='frontLieu'>
                <div class='imagePolaroid' style='background-image:url("`+photoLieu+`")'></div>
                <p class="has-text-centered">`+nomLieu+`</p>
                <div class='voir-plus'></div>
                <a class='button btn-brown'><i class='icon-add'></i></a></div>
              </div>
              <div class='backLieu is-hidden' data-id="`+element['lieu']['id']+`">
                <div class='background-lieu' style='background-image:url("`+photoLieu+`")' ></div>
                <p class="title dark-blue">`+nomLieu+`</p>
                <div class='other-infos'>
                  `+descriptionLieu+tarifLieu+siteWeb+adresseLieu+`
                </div>
              </div>
            </div>
          </div>`;
          $('#listProgram').append(lieu);
        });

        $('nav .back').attr('data-section','list-program');
        if($('nav .back').hasClass('opacity') == false){
          $('nav .back').addClass('opacity');
        }

        /*$('#listProgram .polaroidVisite .button').click(function(){
          console.log("ligne 436");
          var parent = $(this).parent().parent();
          var frontLieu = $(this).parent();
          frontLieu.toggleClass('is-hidden');
          console.log(parent.next());
          $('#listProgram .polaroid').toggleClass('is-hidden');
          parent.next().toggleClass('is-hidden');

          $('#listProgram .title').toggleClass('is-hidden');
          $('#listProgram .infosCreneau').toggleClass('is-hidden');
          $('#listProgram .backLieu .title').toggleClass('is-hidden');
          $('#listProgram').toggleClass('backLieu-open');
          $('nav .back').attr('data-section','see-program');
          if($('nav .back').hasClass('opacity') == true){
            $('nav .back').removeClass('opacity');
          }
        });*/

        $('#listProgram .polaroidVisite').click(function(){
          var frontLieu = $(this).children('.frontLieu');
          frontLieu.toggleClass('is-hidden');
          $('#listProgram .polaroid').toggleClass('is-hidden');
          $(this).next().toggleClass('is-hidden');

          $('#listProgram .title').toggleClass('is-hidden');
          $('#listProgram .infosCreneau').toggleClass('is-hidden');
          $('#listProgram .backLieu .title').toggleClass('is-hidden');
          $('#listProgram').toggleClass('backLieu-open');
          $('nav .back').attr('data-section','see-program');
          if($('nav .back').hasClass('opacity') == true){
            $('nav .back').removeClass('opacity');
          }
        });



      },
      error: function(resultat, statut, erreur){
        console.log('erreur');
        console.log(erreur);
        alert('Une erreur s\'est produite merci de réessayer');
      },
      complete: function(resultat, statut){
        console.log('complete');
        console.log(resultat);
        console.log(statut);
      }
    });
  }
}


var voyage = new Voyage();

// verify if user token exists
if(!localStorage.getItem('token')){
  window.location = "index.html";
}

// DECONNEXION
$('#logout').click(function(){
  localStorage.removeItem('token');
  window.location = "index.html";
});


// if user search a destination, search his city
$("#searchVilles").keyup(function(e){
  if(e.keyCode == 13){
    e.preventDefault();
    e.target.blur();
    return false;
  }
  else {
    var ville = $(this)[0].value;
    var url = "https://www.justine-vincent.fr/echappee/projet_diplome/public/index.php/api/ville/search";
    voyage.searchVilles(url, ville);
  }
});

$("#searchVilles").keypress(function(e){
  if(e.keyCode == 13){
    e.preventDefault();
    e.target.blur();
    return false;
  }
});

// search thematiques
var selectThematiques = $('#listThematiques');
if(selectThematiques != undefined || selectThematiques != null){
  var url = "https://www.justine-vincent.fr/echappee/projet_diplome/public/index.php/api/thematique/list";
  voyage.getThematiquesList(url, selectThematiques)
}

var creneauHeure = $('.creneauHeure');
if(creneauHeure != undefined || creneauHeure != null){
  for(var c=0; c < creneauHeure.length; c++ ){
    for(var i=0; i <= 24; i++){
      var option = document.createElement('option');
      option.appendChild(document.createTextNode(i+"h"));
      option.value = i+":00";
      creneauHeure[c].appendChild(option);
    }
  }
}

// get all day betweek to dates : format date like (0 00/00/000) => Day DD/MM/AAAA
function listJoursCreneaux(dateArrivee, dateDepart){
  var split = dateArrivee.split('/');
  dateArrivee = split[2]+"-"+split[1]+"-"+split[0];
  split = dateDepart.split('/');
  dateDepart = split[2]+"-"+split[1]+"-"+split[0];

  var days = [];

 if(dateArrivee != "" && dateDepart != ""){
  var tempDate = dateArrivee;
  dateDepart = new Date(dateDepart);
  dateArrivee = new Date(dateArrivee);
  tempDate = dateArrivee;

  while(tempDate <= dateDepart){
    tempDate = tempDate.toString();

    var split = tempDate.split(' ');
    tempDate = new Date(tempDate);
    var tempMonth = parseFloat(tempDate.getMonth())+1;

    if(tempMonth<10){
      var month = "0"+tempMonth;
    } else {
      var month = tempMonth;
    }

    if(tempDate.getDate()<10){
      var day = "0"+tempDate.getDate();
    } else {
      var day = tempDate.getDate();
    }

    days.push(tempDate.getDay()+" "+day+"/"+month+"/"+split[3]);

    var day = new Date(split[3], tempDate.getMonth(), split[2], 0,0,0,0);
    tempDate = day.setDate(day.getDate()+1);
    tempDate = new Date(tempDate);
  }
 }

 return days;
}


//get all data in form search to get propositions of places
$('#discoverPlanning').click(function(){
  getAllDatas("", "", "0");
});

//add place in program
$('#addLieu .button').click(function(){
  var slideActive = $('.swiper-slide-active')
  var idLieu = slideActive.data('id');
  var creneauSelected = slideActive.data('creneau');
  $('nav .back').toggleClass('opacity');

  var backLieu = $('.backLieu');
  var dureeVisite = "";
  $.each(backLieu, function(index, element){
    if(element.getAttribute('data-id') == idLieu){
      var button = $(this).find('button.selected');
      dureeVisite = button.val();
    }
  });

  getAllDatas(idLieu, dureeVisite, creneauSelected);

  if($('#listLieux').hasClass('backLieu-open')){
    $('#chooseLieu .title').toggleClass('is-hidden');
    $('#chooseLieu .infosCreneau').toggleClass('is-hidden');
    $('#chooseLieu .backLieu .title').toggleClass('is-hidden');
    $('#listLieux').toggleClass('backLieu-open');
  }

});

// get trend city
var urlVillesTendances = "https://www.justine-vincent.fr/echappee/projet_diplome/public/index.php/api/ville/tendance";
var villesTendances = voyage.searchVillesTendances(urlVillesTendances);

//get data of search form
function getAllDatas(idLieu, dureeVisite, creneauSelected){
  var ville = $('.polaroidLieu.is-active')[0].getAttribute("data-nom");
  var typeCreneau = $('#typesCreneaux .type-creneau.is-active')[0].getAttribute('data-type');
  //par defaut
  if(typeCreneau == "par-defaut"){
    //create each slot with dates added
    var creneauxDemandes = [];
    const element = document.querySelector('#datepickerCreneaux');
    var duree = element.bulmaCalendar.value().split(' - ');
    var dates = listJoursCreneaux(duree[0], duree[1]);
    $.each(dates, function(key,date){
      var day = new Date(date).getDay();
      creneauxDemandes.push(
        [
          date,
          "08:00",
          "12:00"
        ],
        [
          date,
          "14:00",
          "18:00"
        ]
      );
    });
  }
  else {
    //create array with all slot
    var creneauxDemandes = [];
    var creneaux = $('.creneau');
    for(var t=1; t <= creneaux.length; t++){
      var elementDays = document.querySelector('#creneau'+t+' .datepickerDays');
      var elementTimes = document.querySelector('#creneau'+t+' .datepickerTimes');
      var duree = elementTimes.bulmaCalendar.value().split(' - ');
      var day = new Date(elementDays.bulmaCalendar.value()).getDay();
      creneauxDemandes.push(
        [
          day+' '+elementDays.bulmaCalendar.value(),
          duree[0],
          duree[1]
        ]
      );
    }

  }

  //get thematics
  var thematiquesChecked = "";
  $.each($('.thematique.is-active'), function(key, thematique){
    thematiquesChecked = thematiquesChecked+thematique.getAttribute('data-id')+",";
  });
  thematiquesChecked = thematiquesChecked.substring(0,thematiquesChecked.length-1);

  var url = "https://www.justine-vincent.fr/echappee/projet_diplome/public/index.php/api/searchLieuForProgram";
  //search places
  var searchProgram = voyage.searchProgram(url, ville, duree, creneauxDemandes, thematiquesChecked, idLieu, dureeVisite, creneauSelected);
}
