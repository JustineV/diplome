//get today date
var today = new Date();
var date = (today.getMonth()+1)+'/'+today.getDate()+'/'+today.getFullYear();

const elementDays = document.querySelector('.datepickerDays');
const elementTimes = document.querySelector('.datepickerTimes');

// Initialize first calendar to select arrival and departure date
var calendars = bulmaCalendar.attach('#datepickerCreneaux', {
    displayMode: 'dialog',
    isRange: true,
    labelFrom: 'Arrivée',
    labelTo: 'Départ',
		todayLabel: 'Aujourd\'hui',
		startDate: date,
		minDate: date,
		allowSameDayRange: true,
		clearLabel: "Supprimer",
		cancelLabel:"Annuler",
		dateFormat: "DD/MM/YYYY"
  });

const element = document.querySelector('#datepickerCreneaux');
if(element){
  //when first calendar is open
	element.bulmaCalendar.on('select',function(datepicker){
		var myCreneaux = datepicker.data.value().split(' - ');

		if(!elementDays.bulmaCalendar){
      //create first time slot if we have to use it
			var days = bulmaCalendar.attach(elementDays, {
				displayMode: 'dialog',
				labelFrom: 'Arrivée',
				labelTo: 'Départ',
				todayLabel: 'Aujourd\'hui',
				startDate: myCreneaux[0],
				minDate: myCreneaux[0],
				maxDate: myCreneaux[1],
				clearLabel: "Supprimer",
				cancelLabel:"Annuler",
				validateLabel:"Valider",
				dateFormat: "DD/MM/YYYY"
			});

			$('#typesCreneaux .title').toggleClass('is-hidden');
			$('.type-creneau').toggleClass('is-hidden');
			$('#typesCreneaux .button').toggleClass('is-hidden');

			var times = bulmaCalendar.attach('.datepickerTimes', {
				displayMode: 'dialog',
				isRange: true,
				labelFrom: 'Arrivée',
				labelTo: 'Départ',
				todayLabel: 'Aujourd\'hui',
				startDate: date,
				minDate: date,
				allowSameDayRange: true,
				clearLabel: "Modifier",
				cancelLabel:"Annuler",
				validateLabel:"Valider",
				minuteSteps:30
			});
		}

		var startDateFrFormat = myCreneaux[0].split('/');
		var startDate = startDateFrFormat[1]+"/"+startDateFrFormat[0]+"/"+startDateFrFormat[2]+" 08:00:00";
		var endDateFrFormat = myCreneaux[1].split('/');
		var endDate = endDateFrFormat[1]+"/"+endDateFrFormat[0]+"/"+endDateFrFormat[2]+" 09:00:00";

    // for custom slot init date start and min and max to force user to not exceed is departure and arrival dates
		elementDays.bulmaCalendar.value("");
		elementDays.bulmaCalendar.datePicker.min = startDate;
		elementDays.bulmaCalendar.datePicker.max = endDate;
		elementDays.bulmaCalendar.datePicker.start = startDate;
		elementDays.bulmaCalendar.datePicker.clear();
		elementDays.bulmaCalendar.value(startDate);


    elementTimes.bulmaCalendar.timePicker.start = new Date(startDate);
    elementTimes.bulmaCalendar.timePicker.end = new Date(endDate);
    elementTimes.bulmaCalendar.timePicker.startTime = "08:00";
    elementTimes.bulmaCalendar.timePicker.endTime = "09:00";
    elementTimes.bulmaCalendar.refresh();

    elementTimes.bulmaCalendar.timePicker._ui.start.hours.input.value = 08;
    elementTimes.bulmaCalendar.timePicker._ui.start.hours.number.innerHTML = "08";
    elementTimes.bulmaCalendar.timePicker._ui.start.minutes.number.value = 00;
    elementTimes.bulmaCalendar.timePicker._ui.start.minutes.number.innerHTML = "00";
    elementTimes.bulmaCalendar.timePicker._ui.end.hours.input.value = 09;
    elementTimes.bulmaCalendar.timePicker._ui.end.hours.number.innerHTML = "09";
    elementTimes.bulmaCalendar.timePicker._ui.end.minutes.number.value = 00;
    elementTimes.bulmaCalendar.timePicker._ui.end.minutes.number.innerHTML = "00";
    elementTimes.bulmaCalendar.save();
	});
}




// Loop on each calendar initialized
for(var i = 0; i < calendars.length; i++) {
	// Add listener to date:selected event
	calendars[i].on('select', date => {
		console.log(date);
	});
}


$('.addCreneau').click(function(){
  var selectCreneau = document.getElementsByClassName('creneau');
	var selectDays = document.querySelector('.datepickerDays');
	var selectCreneaux = document.querySelector('#datepickerCreneaux');
	console.log(selectCreneaux);
	var myCreneaux = selectCreneaux.bulmaCalendar.value().split(' - ');
	console.log(myCreneaux);
	var startDateFrFormat = myCreneaux[0].split('/');
	var startDate = startDateFrFormat[1]+"/"+startDateFrFormat[0]+"/"+startDateFrFormat[2]+" 08:00:00";
	var endDateFrFormat = myCreneaux[1].split('/');
	var endDate = endDateFrFormat[1]+"/"+endDateFrFormat[0]+"/"+endDateFrFormat[2]+" 09:00:00";
	$('.creneaux').append(`
	<div class="creneau has-text-left" id="creneau`+(selectCreneau.length+1)+`">
		<p class="dark-blue is-uppercase">Créneau `+(selectCreneau.length+1)+`</p>
		<label class="dark-blue">Date: </label><input class="datepickerDays" type="date" />
		<label class="dark-blue">Horaires: </label><input class="datepickerTimes" type="time" />
	</div>
	`);


	var elementDays = document.querySelector("#creneau" + (selectCreneau.length) + " .datepickerDays");
	console.log(myCreneaux[0]);
	var days = bulmaCalendar.attach(elementDays, {
		displayMode: 'dialog',
		labelFrom: 'Arrivée',
		labelTo: 'Départ',
		todayLabel: 'Aujourd\'hui',
		startDate: startDate,
		minDate: startDate,
		maxDate: endDate,
		clearLabel: "Modifier",
		cancelLabel:"Annuler",
		validateLabel:"Valider",
		dateFormat: "DD/MM/YYYY"
	});

	var elementTimes = document.querySelector("#creneau" + (selectCreneau.length) + " .datepickerTimes");
	var times = bulmaCalendar.attach(elementTimes, {
		displayMode: 'dialog',
		isRange: true,
		labelFrom: 'Arrivée',
		labelTo: 'Départ',
		todayLabel: 'Aujourd\'hui',
		startDate: date,
		minDate: date,
		allowSameDayRange: true,
		clearLabel: "Modifier",
		cancelLabel:"Annuler",
		validateLabel:"Valider",
    minuteSteps:30
	});

  elementTimes.bulmaCalendar.timePicker.start = new Date(startDate);
  elementTimes.bulmaCalendar.timePicker.end = new Date(endDate);
  elementTimes.bulmaCalendar.timePicker.startTime = "08:00";
  elementTimes.bulmaCalendar.timePicker.endTime = "09:00";
  elementTimes.bulmaCalendar.refresh();

  elementTimes.bulmaCalendar.timePicker._ui.start.hours.input.value = 08;
  elementTimes.bulmaCalendar.timePicker._ui.start.hours.number.innerHTML = "08";
  elementTimes.bulmaCalendar.timePicker._ui.start.minutes.number.value = 00;
  elementTimes.bulmaCalendar.timePicker._ui.start.minutes.number.innerHTML = "00";
  elementTimes.bulmaCalendar.timePicker._ui.end.hours.input.value = 09;
  elementTimes.bulmaCalendar.timePicker._ui.end.hours.number.innerHTML = "09";
  elementTimes.bulmaCalendar.timePicker._ui.end.minutes.number.value = 00;
  elementTimes.bulmaCalendar.timePicker._ui.end.minutes.number.innerHTML = "00";
  elementTimes.bulmaCalendar.save();
});
