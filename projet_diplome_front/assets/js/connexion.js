/* FILE WHICH CONCERN ONLY LOGIN AND REGISTER FORM */

//Submission of register form
$('#formInscription').submit(function(e){
  e.preventDefault();
  //Get values of form
  var identifiant = $('#formInscription input[name="email"]').val();
  var password = $('#formInscription input[name="password"]').val();
  var confirm = $('#formInscription input[name="confirmation"]').val();
  $('#messageInscription').addClass('is-loading');

  //verify if password and confirm password are the same
  if(password == confirm){
    //obligy to have specials characters, capitals, lowercase and numbers
    var regex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
    if(regex.test(password) == true){
      // verify username not already taken and register in db
      $.ajax({
        url:"https://www.justine-vincent.fr/echappee/projet_diplome/public/index.php/admin/inscription",
        type:"POST",
        data:
          {
            "email":identifiant,
            "password":password
          },
        dataType: "JSON",
        success: function(data)
        {
          $('#messageInscription').removeClass('is-loading');
          if(data != "false"){
            // add token to verify if user already connected the next time
            localStorage.setItem("token", data);
            $('#echappeeDef').removeClass('is-hidden');
            $('#formInscription').addClass('is-hidden');
          }
          else {
            $('#messageInscription').html('<p>Cet identifiant est déjà pris ou votre mot de passe n\'est pas assez sécurisé (Carac maj, min, spéc et numéros)</p>');
          }
        }
      });
    }
    else {
      $('#messageInscription').html('<p>Cet identifiant est déjà pris ou votre mot de passe n\'est pas assez sécurisé (Carac maj, min, spéc et numéros)</p>');
    }
  }
});

//Submission of login form
$('#formConnexion').submit(function(e){
  e.preventDefault();
  var identifiant = $('#formConnexion input[name="email"]').val();
  var password = $('#formConnexion input[name="password"]').val();
  $('#messageConnexion').addClass('is-loading');

  var regex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
  if(regex.test(password) == true){
    $.ajax({
      url:"https://www.justine-vincent.fr/echappee/projet_diplome/public/index.php/admin/connexion",
      type:"POST",
      data:
        {
          "email":identifiant,
          "password":password
        },
      dataType: "JSON",
      success: function(data)
      {
        $('#messageConnexion').removeClass('is-loading');
        if(data != "false"){
          // change or add token for the next time
          localStorage.setItem("token", data);
          $('#pretEchappee').removeClass('is-hidden');
          $('#formConnexion').addClass('is-hidden');
        }
        else {
          $('#messageConnexion').html('<p>Votre identifiant ou votre mot de passe n\'est pas correct</p>');
        }
      },
      error: function(resultat, statut, erreur)
      {
        alert(erreur);
        $('#formConnexion').toggleClass('is-hidden');
        $('#formInscription').toggleClass('is-hidden');
      }
    });
  }
  else {
    $('#messageConnexion').html('<p>Votre identifiant ou votre mot de passe n\'est pas correct</p>');
  }

});
