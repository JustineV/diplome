/* FILE FOR REDIRECT, HIDE, SHOW & ACTIVE */

// when app is load, verify if user connected - has the right token
$(document).ready(function() {
  var url = window.location.pathname;
  var url_explode = url.split('/');

  //verify token
  if(url_explode[url_explode.length-1] == "search_destination.html"){
    $.ajax({
      url:"https://www.justine-vincent.fr/echappee/projet_diplome/public/index.php/user/verifyToken",
      type:"GET",
      data:
        {
          "token": localStorage.getItem('token')
        },
      dataType: "json",
      success: function(response,statut)
      {
        //if no right token, has to connect
        if(response == false){
          window.location = "index.html";
        }
      }
    });
  }

  $(".navbar-burger").click(function() {
    $(".navbar").toggleClass('is-open');
      $(".navbar-burger").toggleClass("is-active");
      $(".navbar-menu").toggleClass("is-active");

  });
});

/* CLICK ON BUTTON */
$('.button').click(function(){
  if($(this).hasClass('is-active') == false){
    $(this).addClass('is-active');
  } else {
    $(this).removeClass('is-active');
  }
});

/* SHOW-HIDE FORM CONNEXION AND INSCRIPTION */
$('.btn-form-connexion').click(function(){
  activeFormsHome($(this), "connexion");
});

$('.btn-form-inscription').click(function(){
  activeFormsHome($(this), "inscription");
});

function activeFormsHome(element,form){
  if(element.hasClass('is-active')){
    if(form == "connexion"){
      $('#formConnexion').removeClass('is-hidden');
    }
    else if(form == "inscription"){
      $('#formInscription').removeClass('is-hidden');
    }
    $('.group-btn-accueil').addClass('is-hidden');
    $('.logo-accueil').removeClass('logo-vertical-center');
  }
  else {
    if(form == "connexion"){
      $('#formConnexion').addClass('is-hidden');
    }
    else if(form == "inscription"){
      $('#formInscription').addClass('is-hidden');
    }
    $('.group-btn-accueil').removeClass('is-hidden');
    $('.logo-accueil').addClass('logo-vertical-center');
  }
}

$('#formConnexion .back > a > i').click(function(){
  $('#formConnexion').addClass('is-hidden');
  $('.group-btn-accueil').removeClass('is-hidden');
  $('.group-btn-accueil .btn-accueil:first-child').removeClass('is-active');
});

$('#formInscription .back > a > i').click(function(){
  $('#formInscription').addClass('is-hidden');
  $('.group-btn-accueil').removeClass('is-hidden');
  $('.group-btn-accueil .btn-accueil:nth-child(2)').removeClass('is-active');
});

$('#pretEchappee a').click(function(){
  window.location = "search_destination.html";
});

$('#echappeeDef a').click(function(){
  window.location = "search_destination.html";
});




/* Choose creneau 'par defaut' or 'personnalisé' */
$('.type-creneau').click(function(){
  $('.type-creneau').toggleClass('is-active');
});

// when slot chose see thematics or custom creneau
$('#typesCreneaux a.button').click(function(){
  $('nav .back').attr('data-section','thematiques');
  var typeCreneau = $('#typesCreneaux .type-creneau.is-active')[0].getAttribute('data-type');
  // if par defaut slot chose
  if(typeCreneau == "par-defaut"){
    $('#typesCreneaux').toggleClass('is-hidden');
    $('#typesCreneaux > p').toggleClass('is-hidden');
    $('#creneauPersonnalise').toggleClass('is-hidden');
    $('#creneauPersonnalise .title').toggleClass('is-hidden');
    $('#chooseThematiques').toggleClass('is-hidden');
    $('.etapes-recherche i:last-child').removeClass('icon-lens-outline').addClass('icon-lens');
  }
  //if custom slot choose
  else {
    if($(this).hasClass('is-active') == true){
      $('.type-creneau').toggleClass('is-hidden');
      $('#typesCreneaux > p').toggleClass('is-hidden');
      $('#creneauPersonnalise .title').toggleClass('is-hidden');
      $('#creneauPersonnalise').toggleClass('is-hidden');
    }
    else {
      $('#typesCreneaux').toggleClass('is-hidden');
      $('#creneauPersonnalise').toggleClass('is-hidden');
      $('#chooseThematiques').toggleClass('is-hidden');
      $('.etapes-recherche i:last-child').removeClass('icon-lens-outline').addClass('icon-lens');
    }
  }
});


/* DISCOVER PLANNING */
$('#discoverPlanning').click(function(){
  $('#chooseThematiques').toggleClass('is-hidden');
  $('.etapes-recherche').toggleClass('is-hidden');
  $('#formSearchProgram').toggleClass('is-hidden');
  $('#chooseLieu').toggleClass('is-hidden');
  $('nav .back').attr('data-section','');
  $('nav .back').toggleClass('opacity');
});


/* BUTTON BACK TO */
$('nav .back').click(function(){
  var attribute = $(this).attr('data-section');
  if(attribute == "backLieu"){
    var dataId = $('.backLieu:not(".is-hidden")').attr('data-id');
    var polaroidVisite = $('.polaroidVisite').toggleClass('is-active');
    $.each(polaroidVisite, function(index, element){
      $(element).toggleClass('is-hidden');
      if(element.getAttribute('data-id') == dataId){
        if($('#listLieux').hasClass('backLieu-open')){
          $('#listLieux').toggleClass('backLieu-open');
          $(this).children('.frontLieu').toggleClass('is-hidden');
          $('.backLieu:not(".is-hidden")').toggleClass('is-hidden');
          $('#chooseLieu .title').toggleClass('is-hidden');
          $('#chooseLieu .infosCreneau').toggleClass('is-hidden');
          $('#chooseLieu .backLieu .title').toggleClass('is-hidden');
          $('#chooseLieu .swiper-button-prev').toggleClass('is-hidden');
          $('#chooseLieu .swiper-button-next').toggleClass('is-hidden');
          $('nav .back').toggleClass('opacity');
        }
      }
    });
    $('nav .back').attr('data-section','');
  }
  else if(attribute == "creneau"){
    $('.polaroidVisite.is-active').toggleClass('is-active');
    $(".etapes-recherche i:nth-child(2)").removeClass('icon-lens').addClass('icon-lens-outline');
    $("#inputSearchVille").toggleClass('is-hidden');
    $('#resultatsLieux').toggleClass('is-hidden');
    $('#typesCreneaux').toggleClass('is-hidden');
    $('nav .back').toggleClass('opacity');
  }
  else if(attribute == "thematiques"){
    $('.polaroidVisite.is-active').toggleClass('is-active');
    $('#typesCreneaux').toggleClass('is-hidden');
    $('#typesCreneaux > p').toggleClass('is-hidden');
    $('#creneauPersonnalise').toggleClass('is-hidden');
    $('#creneauPersonnalise .title').toggleClass('is-hidden');
    $('#chooseThematiques').toggleClass('is-hidden');
    $('.etapes-recherche i:last-child').removeClass('icon-lens').addClass('icon-lens-outline');
    $('nav .back').attr('data-section','creneau');
  }
  else if(attribute == "see-program"){
    var dataId = $('.backLieu:not(".is-hidden")').attr('data-id');
    var polaroidVisite = $('.polaroidVisite').toggleClass('is-active');
    $.each(polaroidVisite, function(index, element){
      $(element).toggleClass('is-hidden');
      if(element.getAttribute('data-id') == dataId){
        if($('#listProgram').hasClass('backLieu-open')){
          $('#listProgram').toggleClass('backLieu-open');
          $(this).children('.frontLieu').toggleClass('is-hidden');
          $('.backLieu:not(".is-hidden")').toggleClass('is-hidden');
          $('#listProgram .title').toggleClass('is-hidden');
          $('#listProgram .infosCreneau').toggleClass('is-hidden');
          $('#listProgram .backLieu .title').toggleClass('is-hidden');
          $('#listProgram .swiper-button-prev').toggleClass('is-hidden');
          $('#listProgram .swiper-button-next').toggleClass('is-hidden');
        }
      }
    });
    $('nav .back').attr('data-section','list-program');
    $('nav .back').addClass('opacity');
  }
});

$('#formSearchProgram').submit(function(){
  e.preventDefault();
});
