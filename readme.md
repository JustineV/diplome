# Échappée

## Echappée mais que c’est ?
C’est une application web qui vous propose un programme de lieux touristiques à visiter pour un court séjour, en fonction de thématiques qui vous intéressent (monuments, jardin, musées…) et de vos disponibilités ... et tout ça en quelques clics.

## Pourquoi utiliser Échappée ?
Certaines applications vous proposent de tout choisir et de tout créer sur-mesure mais cela fait beaucoup de renseignements à aller chercher et donc beaucoup de temps à y consacrer. Pour un tour du monde ça vaut le coup mais pour un week-end ou un voyage de quelques jours il n’y a pas de temps à perdre. C’est pour ça qu’Échappée vous propose des lieux à aller voir et c’est vous qui avez le dernier mot sur les lieux à ajouter à votre programme.
Et si vous nous faites confiance les yeux fermés alors vous avez à juste à cliquer sur “Valider” et c’est parti !
D’autres applications peuvent s’apparenter à Échappée mais nos deux atouts sont que vous choisissez les thématiques susceptibles de vous intéresser et la durée minimum du "séjour" ou disponibilité est d'une heure non pas de 2 ou 3 jours.

## Qui êtes-vous ?
* Que vous soyez dans le cadre d’un **déplacement professionnel** parce que vous êtes commercial par exemple ou parce que vous **partez en séminaire**, il n’est pas facile de pouvoir concilier les horaires de travail avec vos quelques disponibilités.
Echappée vous propose de renseigner vos heures de temps libre pour trouver des lieux touristiques ouverts et qui vous correspondent.

* Que vous soyez **seul**, **en couple** ou **en famille** mais décidé à partir en escapade (parfois à la dernière minute) pendant quelques jours (souvent le temps d’un week-end) Echappée est aussi fait pour vous.
Vous n’avez pas besoin d’aller sur plusieurs applications différentes, un programme est créé selon vos attentes en renseignant vos dates de départ et d’arrivée et vos thématiques.

## Mode d’emploi
Pour utiliser Échappée munissez-vous de votre email et choisissez un mot de passe (ex: Test2020!) pour vous inscrire, cela servira à garder en mémoire vos recherches pour que vous puissiez pouvoir y revenir plus tard.
Puis trois types de données vous sont demandées pour vous construire un programme de visite:
* Votre destination
* Vos créneaux de disponibilités (jours et heures)
* Vos thématiques

**Echappée étant encore en cours de lancement et nous n'avons pas encore recueillis assez de données. Voici les destinations à tester sont Paris et Barcelone car c’est celle où nous avons recueillis le plus de données et pour les thématiques ce sont Monuments et Jardin.**

Ensuite, trois lieux touristiques vous sont proposés, vous devez en sélectionner un à ajouter à votre programme et votre choix peut se faire grâce différentes données (tarif, description …). Une heure de visite est automatiquement sélectionnée mais si vous le souhaitez vous pouvez sélectionner 2h ou ½ journée = 4h.
Lorsque vous avez choisi tous vos lieux touristiques alors vous apercevrez votre programme conçu spécialement pour vous et vous pouvez ainsi reconsulter toutes les données des différents lieux touristiques choisis (description, tarif …) pour par exemple pouvoir réserver ...

## En mode technique
Échappée est une application web basée :
* sur le framework **Symfony** en back-end qui permet de faire les appels à l’api Foursquare puis de traiter les données récupérées et de les insérer en base de données.
* sur la librairie **jQuery** pour le front-end pour sa simplicité à faire des requêtes AJAX et à manipuler le DOM
* et **Bulma** pour l’intégration qui est basée sur Flexbox et qui grâce à certaines de ses extensions est très pertinente pour nos besoins notamment sur les questions de dates et de créneaux personnalisés.

## Comment savoir si les lieux présentés sont pertinents ?
Les données des lieux touristiques sont récupérées à l’aide de l’Api Foursquare puis un traitement est effectué grâce à notre algorithme créé sur-mesure pour proposer des lieux qui ont été vérifiés par Foursquare et/ou le score des utilisateurs est suffisamment important pour être sûr de visiter un lieu touristique de qualité et dont les horaires d’ouverture correspondent aux créneaux et aux thématiques sélectionnés.

Vous souhaitant une agréable échappée !

Lien vers l'application : [https://www.justine-vincent.fr/echappee/projet_diplome_front/](https://www.justine-vincent.fr/echappee/projet_diplome_front/)

Lien vers les maquettes : [https://xd.adobe.com/view/4857a0c9-d7e6-485e-43e0-f9e1f696994a-3e66/](https://xd.adobe.com/view/4857a0c9-d7e6-485e-43e0-f9e1f696994a-3e66/)

Lien vers la modélisation des données : [https://drive.google.com/file/d/1SILVC9rv9zFxAeNNfrnulFH_6fy_VhZk/view?usp=sharing](https://drive.google.com/file/d/1SILVC9rv9zFxAeNNfrnulFH_6fy_VhZk/view?usp=sharing)
