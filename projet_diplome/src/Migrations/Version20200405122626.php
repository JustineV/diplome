<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200405122626 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE thematique CHANGE id id VARCHAR(24) NOT NULL');
        $this->addSql('ALTER TABLE voyage ADD ville_id_id INT NOT NULL');
        $this->addSql('ALTER TABLE voyage ADD CONSTRAINT FK_3F9D8955F0C17188 FOREIGN KEY (ville_id_id) REFERENCES ville (id)');
        $this->addSql('CREATE INDEX IDX_3F9D8955F0C17188 ON voyage (ville_id_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE thematique CHANGE id id VARCHAR(24) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE voyage DROP FOREIGN KEY FK_3F9D8955F0C17188');
        $this->addSql('DROP INDEX IDX_3F9D8955F0C17188 ON voyage');
        $this->addSql('ALTER TABLE voyage DROP ville_id_id');
    }
}
