<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200113174503 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        /*$this->addSql('CREATE TABLE voyage (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE voyage_utilisateur (voyage_id INT NOT NULL, utilisateur_id INT NOT NULL, INDEX IDX_2E08FB1168C9E5AF (voyage_id), INDEX IDX_2E08FB11FB88E14F (utilisateur_id), PRIMARY KEY(voyage_id, utilisateur_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE voyage_lieu (voyage_id INT NOT NULL, lieu_id INT NOT NULL, INDEX IDX_BFC5B5D768C9E5AF (voyage_id), INDEX IDX_BFC5B5D76AB213CC (lieu_id), PRIMARY KEY(voyage_id, lieu_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE voyage_utilisateur ADD CONSTRAINT FK_2E08FB1168C9E5AF FOREIGN KEY (voyage_id) REFERENCES voyage (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE voyage_utilisateur ADD CONSTRAINT FK_2E08FB11FB88E14F FOREIGN KEY (utilisateur_id) REFERENCES utilisateur (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE voyage_lieu ADD CONSTRAINT FK_BFC5B5D768C9E5AF FOREIGN KEY (voyage_id) REFERENCES voyage (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE voyage_lieu ADD CONSTRAINT FK_BFC5B5D76AB213CC FOREIGN KEY (lieu_id) REFERENCES lieu (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE thematique_lieu');
        $this->addSql('ALTER TABLE lieu DROP FOREIGN KEY FK_2F577D59F7E4ECA3');
        $this->addSql('DROP INDEX IDX_2F577D59F7E4ECA3 ON lieu');
        $this->addSql('ALTER TABLE lieu ADD image VARCHAR(255) DEFAULT NULL, DROP id_ville_id, CHANGE latitude latitude DOUBLE PRECISION NOT NULL, CHANGE longitude longitude DOUBLE PRECISION NOT NULL, CHANGE heures_ouv heures_ouv TIME DEFAULT NULL, CHANGE heures_ferm heures_ferm TIME DEFAULT NULL, CHANGE tarif tarif DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE meilleur_horaire ADD horaire_depart TIME NOT NULL, ADD horaire_arrivee TIME NOT NULL, DROP horaire_dep, DROP horaire_arr');
        $this->addSql('ALTER TABLE utilisateur ADD password VARCHAR(255) NOT NULL, CHANGE nom nom VARCHAR(255) DEFAULT NULL, CHANGE prenom prenom VARCHAR(255) DEFAULT NULL, CHANGE latitude latitude DOUBLE PRECISION DEFAULT NULL, CHANGE longitude longitude DOUBLE PRECISION DEFAULT NULL');*/
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE voyage_utilisateur DROP FOREIGN KEY FK_2E08FB1168C9E5AF');
        $this->addSql('ALTER TABLE voyage_lieu DROP FOREIGN KEY FK_BFC5B5D768C9E5AF');
        $this->addSql('CREATE TABLE thematique_lieu (thematique_id INT NOT NULL, lieu_id INT NOT NULL, INDEX IDX_EDAD784D476556AF (thematique_id), INDEX IDX_EDAD784D6AB213CC (lieu_id), PRIMARY KEY(thematique_id, lieu_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE thematique_lieu ADD CONSTRAINT FK_EDAD784D476556AF FOREIGN KEY (thematique_id) REFERENCES thematique (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE thematique_lieu ADD CONSTRAINT FK_EDAD784D6AB213CC FOREIGN KEY (lieu_id) REFERENCES lieu (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE voyage');
        $this->addSql('DROP TABLE voyage_utilisateur');
        $this->addSql('DROP TABLE voyage_lieu');
        $this->addSql('ALTER TABLE lieu ADD id_ville_id INT NOT NULL, DROP image, CHANGE latitude latitude INT NOT NULL, CHANGE longitude longitude INT NOT NULL, CHANGE heures_ouv heures_ouv VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE heures_ferm heures_ferm VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE tarif tarif INT DEFAULT NULL');
        $this->addSql('ALTER TABLE lieu ADD CONSTRAINT FK_2F577D59F7E4ECA3 FOREIGN KEY (id_ville_id) REFERENCES ville (id)');
        $this->addSql('CREATE INDEX IDX_2F577D59F7E4ECA3 ON lieu (id_ville_id)');
        $this->addSql('ALTER TABLE meilleur_horaire ADD horaire_dep VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD horaire_arr VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, DROP horaire_depart, DROP horaire_arrivee');
        $this->addSql('ALTER TABLE utilisateur DROP password, CHANGE nom nom VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE prenom prenom VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE latitude latitude INT DEFAULT NULL, CHANGE longitude longitude INT DEFAULT NULL');
    }
}
