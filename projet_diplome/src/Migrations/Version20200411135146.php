<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200411135146 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE voyage_lieu (id INT AUTO_INCREMENT NOT NULL, voyage_id_id INT NOT NULL, lieu_id_id INT NOT NULL, heure_deb DATETIME NOT NULL, heure_fin DATETIME DEFAULT NULL, INDEX IDX_BFC5B5D775D4A2B8 (voyage_id_id), INDEX IDX_BFC5B5D7BA74394C (lieu_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE voyage_lieu ADD CONSTRAINT FK_BFC5B5D775D4A2B8 FOREIGN KEY (voyage_id_id) REFERENCES voyage (id)');
        $this->addSql('ALTER TABLE voyage_lieu ADD CONSTRAINT FK_BFC5B5D7BA74394C FOREIGN KEY (lieu_id_id) REFERENCES lieu (id)');
        $this->addSql('ALTER TABLE thematique CHANGE id id VARCHAR(24) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE voyage_lieu');
        $this->addSql('ALTER TABLE thematique CHANGE id id VARCHAR(24) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
