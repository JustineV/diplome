<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200126154720 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //$this->addSql('DROP TABLE thematique_lieu');
        //$this->addSql('ALTER TABLE lieu ADD id_ville_id INT NOT NULL');
        //$this->addSql('ALTER TABLE lieu ADD CONSTRAINT FK_2F577D59F7E4ECA3 FOREIGN KEY (id_ville_id) REFERENCES ville (id)');
        //$this->addSql('CREATE INDEX IDX_2F577D59F7E4ECA3 ON lieu (id_ville_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE thematique_lieu (thematique_id INT NOT NULL, lieu_id INT NOT NULL, INDEX IDX_EDAD784D476556AF (thematique_id), INDEX IDX_EDAD784D6AB213CC (lieu_id), PRIMARY KEY(thematique_id, lieu_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE thematique_lieu ADD CONSTRAINT FK_EDAD784D476556AF FOREIGN KEY (thematique_id) REFERENCES thematique (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE thematique_lieu ADD CONSTRAINT FK_EDAD784D6AB213CC FOREIGN KEY (lieu_id) REFERENCES lieu (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE lieu DROP FOREIGN KEY FK_2F577D59F7E4ECA3');
        $this->addSql('DROP INDEX IDX_2F577D59F7E4ECA3 ON lieu');
        $this->addSql('ALTER TABLE lieu DROP id_ville_id');
    }
}
