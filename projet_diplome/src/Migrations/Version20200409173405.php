<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200409173405 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE lieu DROP FOREIGN KEY FK_2F577D593D33E236');
        $this->addSql('ALTER TABLE lieu DROP FOREIGN KEY FK_2F577D59FFDD1184');
        $this->addSql('DROP INDEX IDX_2F577D593D33E236 ON lieu');
        $this->addSql('DROP INDEX IDX_2F577D59FFDD1184 ON lieu');
        $this->addSql('ALTER TABLE lieu DROP heure_deb_id, DROP heure_fin_id');
        $this->addSql('ALTER TABLE thematique CHANGE id id VARCHAR(24) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE lieu ADD heure_deb_id INT DEFAULT NULL, ADD heure_fin_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE lieu ADD CONSTRAINT FK_2F577D593D33E236 FOREIGN KEY (heure_deb_id) REFERENCES voyage (id)');
        $this->addSql('ALTER TABLE lieu ADD CONSTRAINT FK_2F577D59FFDD1184 FOREIGN KEY (heure_fin_id) REFERENCES voyage (id)');
        $this->addSql('CREATE INDEX IDX_2F577D593D33E236 ON lieu (heure_deb_id)');
        $this->addSql('CREATE INDEX IDX_2F577D59FFDD1184 ON lieu (heure_fin_id)');
        $this->addSql('ALTER TABLE thematique CHANGE id id VARCHAR(24) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
