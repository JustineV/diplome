<?php

namespace App\Repository;

use App\Entity\Ville;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Ville|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ville|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ville[]    findAll()
 * @method Ville[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VilleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ville::class);
    }

    public function getAllVilles(){
      $qb = $this->createQueryBuilder('ville');
      $qb->orderBy('ville.pays', 'ASC');

      $query = $qb->getQuery();
      return $query->getResult();
    }

    protected function villeToArray(ville $ville){
      $villeArray = [
        'id' => $ville->getId(),
        'nom' => $ville->getNom(),
        'pays' => $ville->getPays(),
        'url_photo' => $ville->getUrlPhoto()
      ];

      return $villeArray;
    }

    protected function villesToArray($villeEntity){
      $villes = array();
      for($i =0; $i < sizeof($villeEntity); $i++){
        $villes[] = $this->villeToArray($villeEntity[$i]);
      }

      return $villes;
    }

    public function findAllByName($name)
    {
        return $this->createQueryBuilder('ville')
            ->andWhere('ville.nom LIKE :val')
            ->setParameter('val', '%'.$name.'%')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByTendance()
    {
        return $this->createQueryBuilder('ville')
            ->andWhere('ville.tendance = true')
            ->orderBy('ville.id', 'DESC')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByNom($nom)
    {
        return $this->createQueryBuilder('ville')
            ->andWhere('ville.nom = :val')
            ->setParameter('val', $nom)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findByName($name)
    {
        return $this->createQueryBuilder('ville')
            ->andWhere('ville.name = :val')
            ->setParameter('val', $name)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

}
