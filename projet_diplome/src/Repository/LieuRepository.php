<?php

namespace App\Repository;

use App\Entity\Lieu;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Lieu|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lieu|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lieu[]    findAll()
 * @method Lieu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LieuRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lieu::class);
    }

    /**
      * @return Lieu[] Returns an array of Lieu objects
      */

    public function findAll()
    {
        return $this->createQueryBuilder('Lieu')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
      * @return Lieu[] Returns an array of Lieu objects
      */

    public function findByVille($id_ville)
    {
      $qb = $this->createQueryBuilder('lieu'); //automatique a Article car dans le repo article
      $qb->addSelect('ville');
      $qb->leftJoin('article.author','author');
      $qb->orderBy('article.title', 'ASC');
      $query = $qb->getQuery();
      return $query->getResult();
    }


    /**
     * @return Lieu[] Returns an array of Lieu objects
    */

    public function findByIdFoursquare($id_foursquare)
    {

        return $this->createQueryBuilder('lieu')
            ->andWhere('lieu.id_foursquare = :val')
            ->setParameter('val', $id_foursquare)
            ->orderBy('lieu.id', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findById($id)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.id = :val')
            ->setParameter('val', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findByThematiqueAndVille($id_thematique, $id_ville)
    {

      return $this->createQueryBuilder('l')
          ->leftJoin("l.thematique_id","t")
          ->addSelect('t')
          ->leftJoin("l.id_ville","v")
          ->addSelect('v')
          ->orWhere('t.id IN (:theme)')
          ->andWhere('v.nom = :ville')
          ->setParameter('theme', $id_thematique)
          ->setParameter('ville', $id_ville)
          ->getQuery()
          ->getArrayResult()
      ;
    }
}
