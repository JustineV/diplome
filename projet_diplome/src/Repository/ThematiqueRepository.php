<?php

namespace App\Repository;

use App\Entity\Thematique;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Thematique|null find($id, $lockMode = null, $lockVersion = null)
 * @method Thematique|null findOneBy(array $criteria, array $orderBy = null)
 * @method Thematique[]    findAll()
 * @method Thematique[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ThematiqueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Thematique::class);
    }

    /**
    * @return Thematique[] Returns an array of Thematique objects
    */

    public function findById($id)
    {
      return $this->createQueryBuilder('lieu')
          ->andWhere('lieu.id = :val')
          ->setParameter('val', $id)
          ->orderBy('lieu.id', 'ASC')
          ->setMaxResults(1)
          ->getQuery()
          ->getOneOrNullResult()
      ;
    }


    public function getAllThematiques(){
      $qb = $this->createQueryBuilder('thematique');

      $query = $qb->getQuery();
      return $query->getResult();
    }

    protected function thematiqueToArray(thematique $thematique){
      $thematiqueArray = [
        'id' => $thematique->getId(),
        'nom' => $thematique->getNom(),
        'icon' => $thematique->getIcon()
      ];

      return $thematiqueArray;
    }


    protected function thematiquesToArray($thematiqueEntity){
      $thematiques = array();
      for($i =0; $i < sizeof($thematiqueEntity); $i++){
        $thematiques[] = $this->thematiqueToArray($thematiqueEntity[$i]);
      }

      return $thematiques;
    }
}
