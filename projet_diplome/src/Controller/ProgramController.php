<?php

namespace App\Controller;

use App\Entity\Lieu;
use App\Entity\Program;
use App\Entity\Thematique;
use App\Entity\User;
use App\Entity\Ville;
use App\Entity\Voyage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Session;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTimeInterface;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

class ProgramController extends AbstractController
{
    private $session;

    public $lieuSelected;

    /**
     * @Route("/program", name="program")
     */
    public function index()
    {
        return $this->render('program/index.html.twig', [
            'controller_name' => 'ProgramController',
        ]);
    }

    /**
     * @Route("/program/list", name="program")
     */
    public function list(Request $req, SessionInterface $session)
    {
      $this->session = $session;
      $voyage_id = $req->get("voyage_id");

      $encoders = [new XmlEncoder(), new JsonEncoder()];
      $normalizers = [new ObjectNormalizer()];
      $serializer = new Serializer($normalizers, $encoders);

      $programRepository = $this->getDoctrine()->getRepository(Program::class);
      $programsSelected = $programRepository->findByVoyage($voyage_id);
      for($p=0; $p < sizeof($programsSelected); $p++){
        $programsSelected[$p]['lieu']['json'] = json_decode($programsSelected[$p]['lieu']['json'], true);
      }
      $program = $serializer->normalize($programsSelected, 'json');
      return new JsonResponse($program);
    }
}
