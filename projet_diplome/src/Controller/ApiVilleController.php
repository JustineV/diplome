<?php

namespace App\Controller;

use App\Entity\Ville;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\JsonResponse;

class ApiVilleController extends AbstractController
{

  /**
  * @Route("/api/ville", name="api_ville")
  */
  public function index()
  {
    return $this->render('ville/index.html.twig', [
      'controller_name' => 'ApiVilleController',
    ]);
  }

  /**
  * @Route("/api/ville/list", name="ville_list")
  */
  public function villeList()
  {
    $villeRepository = $this->getDoctrine()->getRepository(Ville::class);
    $villeEntity = $villeRepository->findAll();

    $villes = $this->villesToArray($villeEntity);

    $jsonResponse = new JsonResponse($villes);
    $jsonResponse->headers->set("Access-Control-Allow-Origin","*");
    return $jsonResponse;
  }

  /**
  * @Route("/api/ville/search", name="ville_search")
  */
  public function villeSearch(Request $req)
  {
    $villeRepository = $this->getDoctrine()->getRepository(Ville::class);
    $villeEntity = $villeRepository->findAllByName($req->get("ville"));

    $villes = $this->villesToArray($villeEntity);;

    $jsonResponse = new JsonResponse($villes);
    $jsonResponse->headers->set("Access-Control-Allow-Origin","*");
    return $jsonResponse;
  }

  /**
  * @Route("/api/ville/tendance", name="ville_tendance")
  */
  public function villeTendance(Request $req)
  {
    $villeRepository = $this->getDoctrine()->getRepository(Ville::class);
    $villeEntity = $villeRepository->findByTendance();

    $villes = $this->villesToArray($villeEntity);

    $jsonResponse = new JsonResponse($villes);
    $jsonResponse->headers->set("Access-Control-Allow-Origin","*");
    return $jsonResponse;
  }

  protected function villeToArray(Ville $ville){
    $villeArray = [
      'id' => $ville->getId(),
      'nom' => $ville->getNom(),
      'pays' => $ville->getPays(),
      'url_photo' => $ville->getUrlPhoto()
    ];

    return $villeArray;
  }


  protected function villesToArray($villeEntity){
    $villes = array();
    for($i =0; $i < sizeof($villeEntity); $i++){
      $villes[] = $this->villeToArray($villeEntity[$i]);
    }

    return $villes;
  }
}
