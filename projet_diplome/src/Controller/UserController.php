<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Session;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class UserController extends AbstractController
{
    /**
     * @Route("/user/verifyToken", name="user_token")
     */
    public function verifyToken(Request $req)
    {
      $token = $req->get("token");
      $userRepository = $this->getDoctrine()->getRepository(User::class);
      $userEntity = $userRepository->findByToken($token);
      if($userEntity != null){
        return new Response("true");
      }
      return new Response("false");
    }
}
