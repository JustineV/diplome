<?php

namespace App\Controller;

use App\Entity\Thematique;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\JsonResponse;

class ApiThematiqueController extends AbstractController
{

  /**
  * @Route("/api/thematique", name="api_thematique")
  */
  public function index()
  {
    return $this->render('thematique/index.html.twig', [
      'controller_name' => 'ApiThematiqueController',
    ]);
  }

  /**
  * @Route("/api/thematique/list", name="thematique_list")
  */
  public function thematiqueList()
  {
    $thematiqueRepository = $this->getDoctrine()->getRepository(Thematique::class);
    $thematiqueEntity = $thematiqueRepository->findAll();

    $thematiques = $this->thematiquesToArray($thematiqueEntity);

    $jsonResponse = new JsonResponse($thematiques);
    $jsonResponse->headers->set("Access-Control-Allow-Origin","*");
    return $jsonResponse;
  }

  protected function thematiqueToArray(Thematique $thematique){
    $thematiqueArray = [
      'id' => $thematique->getId(),
      'nom' => $thematique->getNom(),
      'icon' => $thematique->getIcon()
    ];

    return $thematiqueArray;
  }


  protected function thematiquesToArray($thematiqueEntity){
    $thematiques = array();
    for($i =0; $i < sizeof($thematiqueEntity); $i++){
      $thematiques[] = $this->thematiqueToArray($thematiqueEntity[$i]);
    }

    return $thematiques;
  }
}
