<?php

namespace App\Controller;

use App\Entity\Lieu;
use App\Entity\Program;
use App\Entity\Thematique;
use App\Entity\User;
use App\Entity\Ville;
use App\Entity\Voyage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Session;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTimeInterface;
use Symfony\Component\Validator\Constraints\DateTime;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

class ApiLieuController extends AbstractController
{
    private $session;

    public $lieuNotSelected;
    /**
     * @Route("/api/lieu", name="api_lieu")
     */
    public function index()
    {
        return $this->render('api_lieu/index.html.twig', [
            'controller_name' => 'ApiLieuController',
        ]);
    }

    /**
     * @Route("/api/searchLieu", name="api_search_lieu")
     */
    public function searchLieu(Request $request){
      $ville = $request->get('ville');
      echo $ville;
    }

    /**
     * @Route("/api/searchLieuForProgram", name="api_search_lieu_for_program")
     * A user search a program & we give some places
     */
    public function searchLieuForProgram(Request $req, SessionInterface $session){

      $this->session = $session;
      $ville = $req->get("ville");
      $duree = $req->get("duree");
      $creneaux = $req->get("creneaux");
      $thematiques = $req->get("thematiques");
      $lieuAdded = $req->get("lieuAdded");
      $dureeVisite= $req->get("dureeVisite");
      $creneauSelected = $req->get("creneauSelected");
      $token = $req->get("token");
      $em = $this->getDoctrine()->getManager();
      $array_thematiques = explode(',',$thematiques);
      $this->session->set('creneau_selected', $creneauSelected*1);

      // if the first place suggest & any place was choosen
      if($lieuAdded == null || $lieuAdded == ""){
        //create new voyage for this user
        $voyage = new Voyage();

        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $userEntity = $userRepository->findByToken($token);
        $voyage->addIdUser($userEntity);

        $villeRepository = $this->getDoctrine()->getRepository(Ville::class);
        $villeEntity = $villeRepository->findByNom($ville);
        $voyage->setVilleId($villeEntity);
        $voyage->setCreneaux($creneaux);
        $em->persist($voyage);
        $em->flush();

        $this->session->set('voyage_id', $voyage->getId());
        $this->session->set('creneau_selected', $creneauSelected);
        $this->session->set('heure_debut', $creneaux[0][1]);
        $this->session->set('heure_fin', $creneaux[0][2]);
      }
      // if user user choose a place before
      else {
        //find lieu saved in db
        $lieuRepository = $this->getDoctrine()->getRepository(Lieu::class);
        $lieuEntity = $lieuRepository->findById(intval($lieuAdded));
        //find voyage corresponding in db
        $voyageRepository = $this->getDoctrine()->getRepository(Voyage::class);
        $voyageEntity = $voyageRepository->findById($this->session->get('voyage_id'));

        // add the place saved by user to the program
        $program = new Program();
        $program->setVoyage($voyageEntity);
        $program->setLieu($lieuEntity);
        $datetime_deb = new \DateTime($this->session->get('heure_debut'));
        $date_creneau_select = explode(" ",$creneaux[$this->session->get('creneau_selected')][0]);
        $creneaux_exploded = explode('/',$date_creneau_select[1]);
        $date_deb = strtotime($creneaux_exploded[2]."-".$creneaux_exploded[1]."-".$creneaux_exploded[0]." ".$this->session->get('heure_debut'));
        $date_fin = strtotime($creneaux_exploded[2]."-".$creneaux_exploded[1]."-".$creneaux_exploded[0]." ".$this->session->get('heure_fin'));
        $heure_fin = strtotime('+'.$dureeVisite.' seconds', $date_deb);
        $datetime_fin = new \DateTime(date('H:i',$heure_fin));
        $program->setHeureDeb($datetime_deb);
        $program->setHeureFin($datetime_fin);
        $em->persist($program);
        $em->flush();
        //when places saved and visit times
        //change the end times in begin times
        $temp_heure_deb = date('Y-m-d H:i',$heure_fin);
        $temp_heure_fin = date('Y-m-d H:i',$date_fin);
        //verify if it's possible to add an other visit after the previous visit in the specific slot
        if($temp_heure_deb < $temp_heure_fin){
          if(date('Y-m-d H:i',strtotime('+3600 seconds', $heure_fin)) <= $temp_heure_fin){
            $this->session->set('heure_debut',date('H:i',$heure_fin));
          }
        }
        //not possible to add anoher visit in the slot
        else {
          $temp_creneau_selected = $this->session->get('creneau_selected');
          $size_creneaux = sizeof($creneaux)-1;
          //verify if exist another slot after the previous
          if($temp_creneau_selected+1 <= $size_creneaux){
            $temp_creneau_selected++;
            $this->session->set('creneau_selected', $temp_creneau_selected);
            $this->session->set('heure_debut', $creneaux[$temp_creneau_selected][1]);
            $this->session->set('heure_fin', $creneaux[$temp_creneau_selected][2]);
          }
          else {
            $response = ["response"=>"Program complete", "voyage_id"=> $this->session->get('voyage_id')];
            return new JsonResponse($response);
          }
        }

      }

      $this->session->set('ville', $ville);
      $this->session->set('duree', $duree);
      $this->session->set('creneaux', $creneaux);
      $this->session->set('thematiques', $thematiques);

      $client = HttpClient::create();
      //searching a new place
      $lieu = new Lieu;
      $propositions = array();
      $program = array();
      $ids = array();

      //search places which correspond to the city and thematics
      $response = $client->request('GET',"https://api.foursquare.com/v2/venues/search", [
        'query' => [
          "client_id" => $_ENV['APP_FOURSQUARE_CLIENT_ID'],
          "client_secret" => $_ENV['APP_FOURSQUARE_CLIENT_SECRET'],
          "near" => $ville,
          "categoryId" => $thematiques,
          "v" => "20200115",
        ]
      ]);
      $jsonResponse = $response->toArray();
      // at the moment the place isn't verify
      $verifyPlace = false;

        while($verifyPlace != true){
          // we have to get 3 suggestions of places
          for($m=0; $m<3; $m++){
            // at the moment not sure that place is not already in the program
            $lieuNotSelected = "false";
            // number of time we search a places corresponding to criterias
            $counter = 0;
            // all infos about the places
            $allInfos = false;

            // at the moment not sure that place is not already in the progra
            while($lieuNotSelected == "false"){
              // number of time > 3  we search a places corresponding to criterias
              // search db
              if($counter >= 3){
                $lieuxRepository = $this->getDoctrine()->getRepository(Lieu::class);
                $lieuxEntity = $lieuxRepository->findByThematiqueAndVille($array_thematiques, $ville);
                //if we don't found places corresponding to the criterias in db
                if($lieuxEntity == null || sizeof($lieuxEntity)==0){
                  $response = ["response"=>"No data", "voyage_id"=> $this->session->get('voyage_id')];
                  return new JsonResponse($response);
                }
                //if places found in db, choose one and sure we have all infos
                $firstPlace = $this->chooseLieu($lieuxEntity);
                $firstPlace = json_decode($firstPlace['json'],true);
                $allInfos = true;
                $counter++;
                if($counter > 10){
                  $response = ["response"=>"Too much calls", "voyage_id"=> $this->session->get('voyage_id')];
                  return new JsonResponse($response);
                }
              }
              else {
                //if places found in db, choose one
                $firstPlace = $this->chooseLieu($jsonResponse["response"]["venues"]);
                //at the moment we don't have all infos
                $allInfos = false;
              }

              //verify if place not already selected in suggestions
              $lieuNotSelected = $this->verifyLieuNotSelected($firstPlace, $ids);
              //if place not in suggestions and not lieu saved before
              if($lieuNotSelected == "true" && $lieuAdded == null){
                $voyageRepository = $this->getDoctrine()->getRepository(Voyage::class);
                $voyageEntity = $voyageRepository->findById($this->session->get('voyage_id'));
              }
              //if place not in suggestions and verify if not in the program
              elseif($lieuNotSelected == "true" && $lieuAdded != null) {
                $voyageRepository = $this->getDoctrine()->getRepository(Voyage::class);
                $voyageEntity = $voyageRepository->findById($this->session->get('voyage_id'));
                $lieuNotSelected  = $this->verifyNotInProgram($firstPlace, $voyageEntity, $program);
              }

              //if place still not be already selected
              if($lieuNotSelected == "true"){
                //if we haven't all infos about the place search them
                if($allInfos == false){
                  $infoPlace = $this->searchInfoPlace($firstPlace['id']);
                }
                else {
                  $infoPlace = $firstPlace;
                }
                $this->session->set('infoPlace', $infoPlace);
                $temp = $this->session->get('infoPlace');
                // have the right format of data and verify not empty
                if(gettype($temp) != "array"){
                  if(gettype($temp) != "string"){
                    $content = $temp->getContent();
                    $infoPlace = json_decode($content, true);
                    if(isset($infoPlace['response']['venue'])){
                      $infoPlace = $infoPlace['response']['venue'];
                    }
                    else {
                      $infoPlace = array();
                      $lieuNotSelected = "false";
                    }
                  }
                  else {
                    $infoPlace = array();
                    $lieuNotSelected = "false";
                  }
                }
                else {
                  if(sizeof($temp) > 0){
                    if($allInfos == false){
                      $infoPlace = $temp[0]['json'];
                    }
                  }
                  else {
                    $infoPlace = array();
                    $lieuNotSelected = "false";
                  }
                }

                if(sizeof($infoPlace) == 0){
                  $lieuNotSelected = "false";
                  $counter++;
                }
                else {
                  //verify place has been verified by Foursquare and/or have very good rating
                  $verifyPlace = $this->verifyPlace($infoPlace, $allInfos);
                  if($verifyPlace == true){
                    //verify that opening hours of place correspod to the slot selected
                    $verifyOpenHours = $this->verifyOpenHours($infoPlace, $creneaux[$this->session->get('creneau_selected')][0], $this->session->get('heure_debut'));
                    //if verification of now and +1h visit are true we can add the place to suggestions
                    if((sizeof($verifyOpenHours) != 0) && ($verifyOpenHours['verification'][0] == true) && $verifyOpenHours['verification'][1] == true && isset($infoPlace['location']['city'])){
                      $lieuNotSelected = "true";

                      if(!isset($infoPlace['location']['address'])){
                        if(isset($infoPlace['location']['formattedAddress'])){
                          $address = implode(' ',$infoPlace['location']['formattedAddress']);
                        }
                        elseif(isset($infoPlace['parent']['location']['formattedAddress'])){
                          $address = implode(' ',$infoPlace['parent']['location']['formattedAddress']);
                        }
                        else {
                          $address = "";
                        }

                      }
                      else {
                        $address = $infoPlace['location']['address'];
                      }
                      $array = [
                        'id_foursquare' => $infoPlace['id'],
                        'nom' => $infoPlace['name'],
                        'description' => (isset($infoPlace['description'])) ? $infoPlace['description'] : "",
                        'latitude' => $infoPlace['location']['lat'],
                        'longitude' => $infoPlace['location']['lng'],
                        'adresse' => $address,
                        'image' => (isset($infoPlace['photos']['groups'][0]['items'][0]['prefix'])) ? $infoPlace['photos']['groups'][0]['items'][0]['prefix'].'500x500'.$infoPlace['photos']['groups'][0]['items'][0]['suffix'] : "",
                        'json' => json_encode($infoPlace),
                        'categories' => $infoPlace['categories'],
                        'city' => (isset($infoPlace['location']['city'])) ? $infoPlace['location']['city'] : ""
                      ];
                      //create lieu
                      $lieuRepository = $this->getDoctrine()->getRepository(Lieu::class);
                      $lieuEntity = $this->addLieu($array);
                      //create or complete programme
                      $content = json_decode($lieuEntity->getContent());
                      $json = $content->json;
                      if(gettype($content->json) == "string"){
                        $json = json_decode($content->json);
                      }
                      $ids[] = $firstPlace['id'];
                      //add place to the suggestions
                      $propositions[] = ["id"=>$content->id,'id_foursquare'=>$content->id_foursquare, 'creneau_deb'=> strtotime($this->session->get('heure_debut')), 'horaires_dispos'=>$verifyOpenHours, 'json' =>$json, 'creneau_selected' => $this->session->get('creneau_selected'), 'time_deb' => $this->session->get('heure_debut')];
                    }
                    else {
                      $lieuNotSelected = "false";
                    }
                  }
                  else {
                    if($allInfos == true){
                      $lieuNotSelected = "true";
                    }
                    else {
                      $lieuNotSelected = "false";
                    }
                  }
                }
              }
              $allInfos = false;
            }
          }
        }

      //when we have 3 places return infos
      $response = $propositions;
      //echo $infoPlace['response']['venue']['name'];
      //print_r($infoPlace['response']['venue']['hours']);
      //$jsonResponse->headers->set("Access-Control-Allow-Origin","*");
      return new JsonResponse($response);
    }

    /**
     * @Route("/admin/lieu/add", name="admin_lieu_add")
     * Function which add aplace in BDD if it's not already in or just get infos
     */
    public function addLieu($array){
        //search if place exist
        $lieuRepository = $this->getDoctrine()->getRepository(Lieu::class);
        $lieuEntity = $lieuRepository->findByIdFoursquare($array['id_foursquare']);
        $arrayLieux = $this->lieuxToArray($lieuEntity);
        //search if city exist
        $villeRepository = $this->getDoctrine()->getRepository(Ville::class);
        $json = json_decode($array['json'],true);
        $villeEntity = $villeRepository->findByNom($json['location']['city']);
        if($villeEntity == null){
          $villeRepository->findByName($json['location']['city']);
        }

        //if place don't exist
        if(sizeof($arrayLieux) == 0) {
          //insérer un lieu avec les catégories correspondantes dans la bdd
          $em = $this->getDoctrine()->getManager();
          $lieu = new Lieu();
          $lieu->setIdFoursquare($array['id_foursquare']);
          $lieu->setNom($array['nom']);
          $lieu->setDescription($array['description']);
          $lieu->setLatitude($array['latitude']);
          $lieu->setLongitude($array['longitude']);
          $lieu->setAdresse($array['adresse']);
          $lieu->setImage($array['image']);
          $lieu->setJson($array['json']);
          $lieu->setIdVille($villeEntity);
          // Ajout de toutes les catégories correspondantes au lieu dans la table lieu_thematique
          foreach($array['categories'] as $categorie){
            $thematiqueRepository = $this->getDoctrine()->getRepository(Thematique::class);
            $thematiqueEntity = $thematiqueRepository->findById($categorie['id']);
            if($thematiqueEntity != null){
              $lieu->addThematiqueId($thematiqueEntity);
            }
          }

          $em->persist($lieu);
          $em->flush();

          $response = new Response();
          $response->setContent(json_encode([
            'id' => $lieu->getId(),
            'id_foursquare' => $array['id_foursquare'],
            'json' => $array['json']
          ]));
          $response->headers->set('Content-Type', 'application/json');
          return $response;
        }
        $response = new Response();
        $response->setContent(json_encode([
          'id' => $arrayLieux[0]['id'],
          'id_foursquare' => $arrayLieux[0]['id_foursquare'],
          'json' => $arrayLieux[0]['json']
        ]));
        $response->headers->set('Content-Type', 'application/json');
        return $response;

    }



    /**
     * @Route("/admin/lieu/choose", name="admin_lieu_choose")
     * In the list of places, we choose one of them
     */
    public function chooseLieu($list){
      $randomNumber = rand(0,sizeof($list)-1);
      return $list[$randomNumber];
    }

    /**
     * @Route("/admin/lieu/verify-not-selected", name="admin_lieu_verify-not-selected")
     * Verify that the place chosen isn't not in the suggestion for this slot
     */
    public function verifyLieuNotSelected($list, $lieux){
      if(sizeof($lieux) != 0){
        for($i=0; $i < sizeof($lieux); $i++){
          if($lieux[$i] == $list['id']){
            return "false";
          }
        }
        return "true";
      }
      else {
        return "true";
      }
    }

    /**
     * @Route("/admin/lieu/verify-not-in-program", name="admin_lieu_verify-not-in-program")
     * Verify that the place is not in the program
     */
    public function verifyNotInProgram($place, $voyage, $program){
      $programRepository = $this->getDoctrine()->getRepository(Program::class);
      $lieuxSelected = $programRepository->findByVoyage($voyage->getId());
      foreach($lieuxSelected as $lieuNotSelected){
        if($lieuNotSelected['lieu']['id_foursquare'] == $place['id']){
          return "false";
        }
      }
      return "true";
    }

    /**
     * @Route("/admin/lieu/info", name="admin_lieu_info")
     * Request on api to search all informations on a place
     */
    public function searchInfoPlace($idPlace){
      $client = HttpClient::create();

      $response = $client->request('GET',"https://api.foursquare.com/v2/venues/".$idPlace, [
        'query' => [
          "client_id" => "YJY5REYKOY4UV0HCHLXMME5EHDLEC14VCYCK5FJAL1ECBV1Q",
          "client_secret" => "DNURKSXJXEX4RGPXF0CQ2EOLYGQJDOCJFRNVENVNPTUT2SCS",
          "v" => "20200115",
          "locale" => "fr",
        ]
      ]);

      $statusCode = $response->getStatusCode();
      if($statusCode == 429 || $statusCode == 500){
        $lieuRepository = $this->getDoctrine()->getRepository(Lieu::class);
        $lieuEntity = $lieuRepository->findByIdFoursquare($idPlace);
        $arrayLieux = $this->lieuxToArray($lieuEntity);
        if(isset($arrayLieux[0]['json'])){
          return new Response($arrayLieux[0]['json']);
        }
        else {
          return new Response("");
        }
      }

      $responseArray = $response->toArray();
      return new JsonResponse($responseArray);
    }

    /**
     * @Route("/admin/lieu/verify", name="admin_lieu_verify")
     * Verified if the place has been verified by Foursquare and/or rating are very goof
     */
    public function verifyPlace($place, $allInfos){
      if(isset($place['verified']) && $place['verified'] == true){
        if(isset($place['rating'])){
          if($place['rating'] >= 7){
            $verification = true;
          }
          else {
            $verification = false;
          }
        }
        else {
          $verification = false;
        }
      }
      else if(isset($place['rating']) && $place['rating'] >= 7){
        $verification = true;
      }
      else{
        $verification = false;
      }

      return $verification;
    }

    /**
     * @Route("/admin/lieu/verifyOpenHours", name="admin_lieu_verify_open_hours")
     * Vérifier que les heures d'ouverture du lieu touristique correspondent aux créneaux rentrés par l'utilisateur
     */
    public function verifyOpenHours($place, $jour, $heure_deb){
      //verfication correspond of opening hours of place correspond to slot selected
      //verification_option correspond if user choose to has a duration visit which exceed his slot
      //key of arrays correspond to
      //0: now
      //1: +1h
      //2: +2h
      //3: +4h
      //4: before 18pm
      $verifications = array("verification" => array_fill(0,5,false), "verification_option" => array_fill(0,5,false));
      /*$creneaux = array(
        0 => "0 2020-02-16",
        1 => "9:00",
        2 => "10:00"
      );
      $place = array(
        'hours' => array(
          'timeframes' => array(
            0 => array(
                    'days' => 'Mon–Sun',
                    'includesToday' => 1,
                    'open' => array(
                            0 => array(
                                    'renderedTime' => '9:00 AM–1:00 AM'
                                )

                        )

                )
              )
            )
          );*/
      //if the place have open hours
      if(isset($place['hours']['timeframes'])){
        $place = $place['hours']['timeframes'];
        $day = "";
        //create array with all day of a week
        $arrayDay = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'];
        // copied day choose in slot by his string
        switch (substr($jour,0,1)) {
          case 0:
            $day = $arrayDay[6];
            break;
          case 1:
            $day = $arrayDay[0];
            break;
          case 2:
            $day = $arrayDay[1];
            break;
          case 3:
            $day = $arrayDay[2];
            break;
          case 4:
            $day = $arrayDay[3];
            break;
          case 5:
            $day = $arrayDay[4];
            break;
          case 6:
            $day = $arrayDay[5];
            break;
          default:
            $day = "";
            break;
        }

        //slot of opening times of places
        //ex: Mon-Fri
        //put in a array all intermediary days
        //response: mon, tue, wed ... to fri
        for($i=0; $i < sizeof($place); $i++){
          $arrayDay = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'];
          //if ex: Mon-Fri
          if(strlen($place[$i]['days']) > 3){
            //most of times days separated by -
            //so explode the 2 days
            $arrayExplodeDay = explode('–',$place[$i]['days']);
            if(sizeof($arrayExplodeDay) == 1){
              //sometimes times days separated by ,
              $arrayExplodeDay = explode(', ',$place[$i]['days']);
              //if it's other return
              if(sizeof($arrayExplodeDay) == 1){
                $verifications = array("verification" => array(false, false, false, false, false), "verification_option" => array(false, false, false, false, false));
                return $verifications;
              }
            }
            $indice_day_first = 0;
            //$indice_day_last = sizeof($place);
            $indice_day_last = sizeof($arrayDay)-1;
            //find first day of the opening times and remove days before which don't correspond
            while(($arrayExplodeDay[0] != $arrayDay[$indice_day_first]) && ($indice_day_first < sizeof($arrayDay))) {
              unset($arrayDay[$indice_day_first]);
              $indice_day_first++;
            }
            //find last day of the opening times and remove days after which don't correspond
            while(isset($arrayDay[$indice_day_last]) && $arrayExplodeDay[1] != $arrayDay[$indice_day_last] && $indice_day_last > 0){
              unset($arrayDay[$indice_day_last]);
              $indice_day_last--;
            }
            //we have verify the day now we verify opening hours
            $verifications = $this->verifyOpenHoursForADay($jour, $heure_deb, $place[$i]['open'], $arrayDay);
          }
          //if it's one day
          else{
            if($day == $place[$i]['days']){
              $verifications = $this->verifyOpenHoursForADay($jour, $heure_deb, $place[$i], $arrayDay);
            }
          }
        }
      }
      //if place don't have opening hours
      else {
        //open every time but have to verify if it's including in slot
        $verifications = array('verification' => array_fill(0,5,true), 'verification_option' => array_fill(0,5,false));
        $heure_deb = strtotime($heure_deb);
        $verifications['verification_option'][0] = true;
        if($heure_deb+3600 <= strtotime($this->session->get('heure_fin'))){
          $verifications['verification_option'][1] = true;
        }
        if($heure_deb+7200 <= strtotime($this->session->get('heure_fin'))){
          $verifications['verification_option'][2] = true;
        }
        if($heure_deb+14400 <= strtotime($this->session->get('heure_fin'))){
          $verifications['verification_option'][3] = true;
        }
        if($heure_deb <= strtotime("13:01")){
          if(strtotime("18:00") <= strtotime($this->session->get('heure_fin'))){
            $verifications['verification_option'][4] = true;
          }
        }
      }

      return $verifications;
    }

    /**
     * @Route("/admin/lieu/verifyOpenHoursForADay", name="admin_lieu_verify_open_hours_for_a_day")
     *
     */
   public function verifyOpenHoursForADay($jour, $heure_deb, $place_horaires, $day){
      /*$creneaux = array(
        0 => "0 2020-02-16",
        1 => "11:00",
        2 => "12:00"
      );
      $place_horaires = array(
            0 => array('renderedTime' => '9:00 AM–1:00 AM')
          );
      $day = "Sun";*/
      //for all opening slot of the place
      for($i = 0; $i < sizeof($place_horaires); $i++){
        $this->session->set('test', $place_horaires[$i]);
        // hours are most of the time separated by -
        $horaires = explode('–', $place_horaires[$i]['renderedTime']);
        $horaires[0] = date('G:i', strtotime($horaires[0]));
        if(isset($horaires[1])){
          //if $horaires[0] must be not like 00:00 AM
          if(strlen($horaires[0])<4 || strlen($horaires[1])<4){
            return false;
          }
          //if last hours is between 00:00 - 06:00
          //change by 23:59 to have any problem with days
          if($horaires[1] < date('G:i', strtotime('06:00 AM'))){
            $horaires[1] = date('G:i', strtotime('23:59 PM'));
          }
          //verify if one hour before the closing times correspond to begin slot selected
          $horaires[1] = date('G:i', strtotime($horaires[1])-strtotime('01:00'));
          $heure_deb = strtotime($heure_deb);
          $verification = array_fill(0, 5, false);
          $verification_option = array_fill(0, 5, false);
          //if begin slot selected > opening times  and < 1h before closing times places
          if(($heure_deb >= strtotime($horaires[0])) && ($heure_deb <= strtotime($horaires[1]))){
            $verification[0] = true;
            $verification_option[0] = true;
            //verify closing times for many duration visit to know if
            //possible to propose visit +1h / +2h ...
            if(strtotime($heure_deb+3600) <= strtotime($horaires[1])){
              $verification[1] = true;
              if($heure_deb+3600 <= strtotime($this->session->get('heure_fin'))){
                $verification_option[1] = true;
              }
            }
            if(strtotime($heure_deb+7200) <= strtotime($horaires[1])){
              $verification[2] = true;
              if($heure_deb+7200 <= strtotime($this->session->get('heure_fin'))){
                $verification_option[2] = true;
              }
            }
            if(strtotime($heure_deb+14400) <= strtotime($horaires[1])){
              $verification[3] = true;
              if($heure_deb+14400 <= strtotime($this->session->get('heure_fin'))){
                $verification_option[3] = true;
              }
            }
            if(strtotime("18:00") <= strtotime($horaires[1])){
              $verification[4] = true;
              if($heure_deb <= strtotime("13:01")){
                if(strtotime("18:00") <= strtotime($this->session->get('heure_fin'))){
                  $verification_option[4] = true;
                }
              }
            }
          }
        }
        //if isn't other format
        else {
          $verification = array_fill(0, 5, false);
          $verification_option = array_fill(0, 5, false);
          if($place_horaires[$i]['renderedTime'] = "24 Hours"){
            $heure_deb = strtotime($heure_deb);
            $verification[0] = true;
            $verification_option[0] = true;
            if(strtotime($heure_deb+3600) <= $this->session->get('heure_fin')){
              $verification[1] = true;
              if($heure_deb+3600 <= strtotime($this->session->get('heure_fin'))){
                $verification_option[1] = true;
              }
            }
            if(strtotime($heure_deb+7200) <= strtotime($this->session->get('heure_fin'))){
              $verification[2] = true;
              if($heure_deb+7200 <= strtotime($this->session->get('heure_fin'))){
                $verification_option[2] = true;
              }
            }
            if(strtotime($heure_deb+14400) <= strtotime($this->session->get('heure_fin'))){
              $verification[3] = true;
              if($heure_deb+14400 <= strtotime($this->session->get('heure_fin'))){
                $verification_option[3] = true;
              }
            }
            if(strtotime("18:00") <= strtotime($this->session->get('heure_fin'))){
              $verification[4] = true;
              if($heure_deb <= strtotime("13:01")){
                if(strtotime("18:00") <= strtotime($this->session->get('heure_fin'))){
                  $verification_option[4] = true;
                }
              }
            }
          }
        }
      }
      return array('verification' => $verification, 'verification_option' => $verification_option);
    }

    /**
     * @Route("/admin/lieu/getDurationVisit", name="admin_lieu_get_duration_visit")
     * Get an average duration of visiting this place
     */
    public function getDurationVisit($infoPlace){
      return 3600;
    }


    /**
     * @Route("/admin/lieu/removeVisitNotVerified", name="admin_remove_visit")
     * Get an average duration of visiting this place
     */
    public function removeVisitNotVerified(){
      $lieuRepository = $this->getDoctrine()->getRepository(Lieu::class);
      $lieuEntity = $lieuRepository->findAll();
      $arrayLieux = $this->lieuxToArray($lieuEntity);
      foreach ($arrayLieux as $arrayLieu){
        if($arrayLieu['json'] == null){
          $lieuEntity2 = $lieuRepository->findById($arrayLieu['id']);
          $em = $this->getDoctrine()->getManager();
          $em->remove($lieuEntity2);
          $em->flush();
        }
        else {
          if(isset($arrayLieu['json']['verified'])){
            if($arrayLieu['json']['verified'] == false ){
              if(!isset($arrayLieu['json']['rating'] ) ||  ($arrayLieu['json']['rating'] < 7)){
                $lieuEntity2 = $lieuRepository->findById($arrayLieu['id']);
                $em = $this->getDoctrine()->getManager();
                $em->remove($lieuEntity2);
                $em->flush();
              }
            }
          }
          else {
            $lieuEntity2 = $lieuRepository->findById($arrayLieu['id']);
            $em = $this->getDoctrine()->getManager();
            $em->remove($lieuEntity2);
            $em->flush();
          }
        }
      }
    }


    /*
     * Important informations of a place in an array
     */
    protected function lieuToArray(Lieu $lieu){
      $lieuArray = [
        'id' => $lieu->getId(),
        'nom' => $lieu->getNom(),
        'description' => $lieu->getDescription(),
        'latitude' => $lieu->getLatitude(),
        'longitude' => $lieu->getLongitude(),
        'adresse' => $lieu->getAdresse(),
        'image' => $lieu->getImage(),
        'json' => $lieu->getJson(),
        'id_foursquare' => $lieu->getIdFoursquare(),
        'categories' => $lieu->getThematiqueId()
      ];

      return $lieuArray;
    }

    /*
     * Get a list of place and put them in an array
     */
    protected function lieuxToArray($lieuEntity){
      $lieux = array();
      for($i =0; $i < sizeof($lieuEntity); $i++){
        $lieux[] = $this->lieuToArray($lieuEntity[$i]);
      }

      return $lieux;
    }
}
