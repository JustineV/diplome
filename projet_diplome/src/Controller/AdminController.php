<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminController extends AbstractController
{
  /**
   * @var UserPasswordEncoderInterface
   */
    private $encoder;

    /**
     * @Route("/admin", name="admin")
     */
    public function index()
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

    /**
     * @Route("/admin/inscription", name="admin_inscription")
     */
    public function inscription(Request $req, UserPasswordEncoderInterface $encoder)
    {
      $email = $req->request->get("email");
      $password = $req->request->get("password");

      $userRepository = $this->getDoctrine()->getRepository(User::class);
      $userEntity = $userRepository->findByEmail($email);
      if(sizeof($userEntity) == 0 || $userEntity == NULL) {
        $em = $this->getDoctrine()->getManager();
        $user = new User();
        $encoded = $encoder->encodePassword($user, $password);
        $user->setEmail($email);
        $user->setPassword($encoded);
        $token = sha1(random_bytes(5));
        $user->setToken($token);

        $em->persist($user);
        $em->flush();
        return new JsonResponse($token);
      }
      else {
        return new JsonResponse('false');
      }
    }

    /**
     * @Route("/admin/connexion", name="admin_connexion")
     */
    public function connexion(Request $req, UserPasswordEncoderInterface $encoder)
    {
      $email = $req->get("email");
      $password = $req->get("password");

      $em = $this->getDoctrine()->getManager();
      $user = new User();
      $userRepository = $this->getDoctrine()->getRepository(User::class);

      $user = $userRepository->findByEmail($email);

      if($user){
        if($encoder->isPasswordValid($user, $password)){
          $token = sha1(random_bytes(5));
          $user->setToken($token);
          $em->persist($user);
          $em->flush();
          return new JsonResponse($token);
        }
        else {
        }
      }

      return new JsonResponse('false');
    }
}
