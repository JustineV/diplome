<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LieuRepository")
 */
class Lieu
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="float")
     */
    private $latitude;

    /**
     * @ORM\Column(type="float")
     */
    private $longitude;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $heures_ouv;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $heures_ferm;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $tarif;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ville", inversedBy="lieux")
     * @ORM\JoinColumn(nullable=true)
     */
    private $id_ville;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $id_foursquare;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $json = [];

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Thematique", inversedBy="lieu_id")
     */
    private $thematique_id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Program", mappedBy="lieu")
     */
    private $programs;

    public function __construct()
    {
        $this->thematique_id = new ArrayCollection();
        $this->programs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getHeuresOuv(): ?\DateTimeInterface
    {
        return $this->heures_ouv;
    }

    public function setHeuresOuv(?\DateTimeInterface $heures_ouv): self
    {
        $this->heures_ouv = $heures_ouv;

        return $this;
    }

    public function getHeuresFerm(): ?\DateTimeInterface
    {
        return $this->heures_ferm;
    }

    public function setHeuresFerm(?\DateTimeInterface $heures_ferm): self
    {
        $this->heures_ferm = $heures_ferm;

        return $this;
    }

    public function getTarif(): ?float
    {
        return $this->tarif;
    }

    public function setTarif(?float $tarif): self
    {
        $this->tarif = $tarif;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getIdVille(): ?Ville
    {
        return $this->id_ville;
    }

    public function setIdVille(?Ville $id_ville): self
    {
        $this->id_ville = $id_ville;

        return $this;
    }

    public function getIdFoursquare(): ?string
    {
        return $this->id_foursquare;
    }

    public function setIdFoursquare(string $id_foursquare): self
    {
        $this->id_foursquare = $id_foursquare;

        return $this;
    }

    public function getJson(): ?string
    {
        return $this->json;
    }

    public function setJson(?string $json): self
    {
        $this->json = $json;

        return $this;
    }

    /**
     * @return Collection|Thematique[]
     */
    public function getThematiqueId(): Collection
    {
        return $this->thematique_id;
    }

    public function addThematiqueId(Thematique $thematiqueId): self
    {
        //var_dump(!$this->thematique_id->contains($thematiqueId));
        if (!$this->thematique_id->contains($thematiqueId)) {
            $this->thematique_id[] = $thematiqueId;
        }

        return $this;
    }

    public function removeThematiqueId(Thematique $thematiqueId): self
    {
        if ($this->thematique_id->contains($thematiqueId)) {
            $this->thematique_id->removeElement($thematiqueId);
        }

        return $this;
    }

    /**
     * @return Collection|Program[]
     */
    public function getPrograms(): Collection
    {
        return $this->programs;
    }

    public function addProgram(Program $program): self
    {
        if (!$this->programs->contains($program)) {
            $this->programs[] = $program;
            $program->setLieu($this);
        }

        return $this;
    }

    public function removeProgram(Program $program): self
    {
        if ($this->programs->contains($program)) {
            $this->programs->removeElement($program);
            // set the owning side to null (unless already changed)
            if ($program->getLieu() === $this) {
                $program->setLieu(null);
            }
        }

        return $this;
    }
}
