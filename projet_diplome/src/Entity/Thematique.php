<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ThematiqueRepository")
 */
class Thematique
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="string", length=24)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=24, nullable=true)
     */
    private $parent_id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Lieu", mappedBy="thematique_id")
     */
    private $lieu_id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $icon;

    public function __construct()
    {
        $this->lieu_id = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getParentId(): ?string
    {
        return $this->parent_id;
    }

    public function setParentId(?int $parent_id): self
    {
        $this->parent_id = $parent_id;

        return $this;
    }

    /**
     * @return Collection|Lieu[]
     */
    public function getLieuId(): Collection
    {
        return $this->lieu_id;
    }

    public function addLieuId(Lieu $lieuId): self
    {
        if (!$this->lieu_id->contains($lieuId)) {
            $this->lieu_id[] = $lieuId;
            $lieuId->addThematiqueId($this);
        }

        return $this;
    }

    public function removeLieuId(Lieu $lieuId): self
    {
        if ($this->lieu_id->contains($lieuId)) {
            $this->lieu_id->removeElement($lieuId);
            $lieuId->removeThematiqueId($this);
        }

        return $this;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(?string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }
}
