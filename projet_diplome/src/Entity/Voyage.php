<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VoyageRepository")
 */
class Voyage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User")
     */
    private $id_user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ville", inversedBy="voyages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ville_id;

    /**
     * @ORM\Column(type="array")
     */
    private $creneaux = [];

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Program", mappedBy="voyage")
     */
    private $programs;

    public function __construct()
    {
        $this->id_user = new ArrayCollection();
        $this->id_lieu = new ArrayCollection();
        $this->programs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|User[]
     */
    public function getIdUser(): Collection
    {
        return $this->id_user;
    }

    public function addIdUser(User $idUser): self
    {
        if (!$this->id_user->contains($idUser)) {
            $this->id_user[] = $idUser;
        }

        return $this;
    }

    public function removeIdUser(User $idUser): self
    {
        if ($this->id_user->contains($idUser)) {
            $this->id_user->removeElement($idUser);
        }

        return $this;
    }

    public function getVilleId(): ?Ville
    {
        return $this->ville_id;
    }

    public function setVilleId(?Ville $ville_id): self
    {
        $this->ville_id = $ville_id;

        return $this;
    }

    public function getCreneaux(): ?array
    {
        return $this->creneaux;
    }

    public function setCreneaux(array $creneaux): self
    {
        $this->creneaux = $creneaux;

        return $this;
    }

    /**
     * @return Collection|Program[]
     */
    public function getPrograms(): Collection
    {
        return $this->programs;
    }

    public function addProgram(Program $program): self
    {
        if (!$this->programs->contains($program)) {
            $this->programs[] = $program;
            $program->setVoyage($this);
        }

        return $this;
    }

    public function removeProgram(Program $program): self
    {
        if ($this->programs->contains($program)) {
            $this->programs->removeElement($program);
            // set the owning side to null (unless already changed)
            if ($program->getVoyage() === $this) {
                $program->setVoyage(null);
            }
        }

        return $this;
    }


}
